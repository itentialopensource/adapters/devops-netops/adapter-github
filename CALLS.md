## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for GitHub System. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for GitHub System.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the GitHub System. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getEmojis(callback)</td>
    <td style="padding:15px">Lists all the emojis available to use on GitHub.</td>
    <td style="padding:15px">{base_path}/{version}/emojis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeeds(callback)</td>
    <td style="padding:15px">List Feeds.
GitHub provides several timeline resources in Atom format. The Feeds API
 lists all the feeds available to the authenticating user.</td>
    <td style="padding:15px">{base_path}/{version}/feeds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGists(body, callback)</td>
    <td style="padding:15px">Create a gist.</td>
    <td style="padding:15px">{base_path}/{version}/gists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGistsId(id, callback)</td>
    <td style="padding:15px">Delete a gist.</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGistsId(id, callback)</td>
    <td style="padding:15px">Get a single gist.</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchGistsId(id, body, callback)</td>
    <td style="padding:15px">Edit a gist.</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGistsIdComments(id, body, callback)</td>
    <td style="padding:15px">Create a commen</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGistsIdCommentsCommentId(id, commentId, callback)</td>
    <td style="padding:15px">Delete a comment.</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/comments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGistsIdCommentsCommentId(id, commentId, callback)</td>
    <td style="padding:15px">Get a single comment.</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/comments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchGistsIdCommentsCommentId(id, commentId, body, callback)</td>
    <td style="padding:15px">Edit a comment.</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/comments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGistsIdForks(id, callback)</td>
    <td style="padding:15px">Fork a gist.</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/forks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGistsIdStar(id, callback)</td>
    <td style="padding:15px">Unstar a gist.</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/star?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGistsIdStar(id, callback)</td>
    <td style="padding:15px">Check if a gist is starred.</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/star?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putGistsIdStar(id, callback)</td>
    <td style="padding:15px">Star a gist.</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/star?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGitignoreTemplates(callback)</td>
    <td style="padding:15px">Listing available templates.
List all templates available to pass as an option when creating a repository.</td>
    <td style="padding:15px">{base_path}/{version}/gitignore/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGitignoreTemplatesLanguage(language, callback)</td>
    <td style="padding:15px">Get a single template.</td>
    <td style="padding:15px">{base_path}/{version}/gitignore/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLegacyIssuesSearchOwnerRepositoryStateKeyword(keyword, state = 'open', owner, repository, callback)</td>
    <td style="padding:15px">Find issues by state and keyword.</td>
    <td style="padding:15px">{base_path}/{version}/legacy/issues/search/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLegacyReposSearchKeyword(keyword, order = 'desc', language, startPage, sort = 'updated', callback)</td>
    <td style="padding:15px">Find repositories by keyword. Note, this legacy method does not follow the v3 pagination pattern. This method returns up to 100 results per page and pages can be fetched using the start_page parameter.</td>
    <td style="padding:15px">{base_path}/{version}/legacy/repos/search/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLegacyUserEmailEmail(email, callback)</td>
    <td style="padding:15px">This API call is added for compatibility reasons only.</td>
    <td style="padding:15px">{base_path}/{version}/legacy/user/email/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLegacyUserSearchKeyword(keyword, order = 'desc', startPage, sort = 'updated', callback)</td>
    <td style="padding:15px">Find users by keyword.</td>
    <td style="padding:15px">{base_path}/{version}/legacy/user/search/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMarkdown(body, callback)</td>
    <td style="padding:15px">Render an arbitrary Markdown document</td>
    <td style="padding:15px">{base_path}/{version}/markdown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMarkdownRaw(callback)</td>
    <td style="padding:15px">Render a Markdown document in raw mode</td>
    <td style="padding:15px">{base_path}/{version}/markdown/raw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMeta(callback)</td>
    <td style="padding:15px">This gives some information about GitHub.com, the service.</td>
    <td style="padding:15px">{base_path}/{version}/meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNotifications(body, callback)</td>
    <td style="padding:15px">Mark as read.
Marking a notification as "read" removes it from the default view on GitHub.com.</td>
    <td style="padding:15px">{base_path}/{version}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotificationsThreadsId(id, callback)</td>
    <td style="padding:15px">View a single thread.</td>
    <td style="padding:15px">{base_path}/{version}/notifications/threads/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNotificationsThreadsId(id, callback)</td>
    <td style="padding:15px">Mark a thread as read</td>
    <td style="padding:15px">{base_path}/{version}/notifications/threads/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNotificationsThreadsIdSubscription(id, callback)</td>
    <td style="padding:15px">Delete a Thread Subscription.</td>
    <td style="padding:15px">{base_path}/{version}/notifications/threads/{pathv1}/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotificationsThreadsIdSubscription(id, callback)</td>
    <td style="padding:15px">Get a Thread Subscription.</td>
    <td style="padding:15px">{base_path}/{version}/notifications/threads/{pathv1}/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNotificationsThreadsIdSubscription(id, body, callback)</td>
    <td style="padding:15px">Set a Thread Subscription.
This lets you subscribe to a thread, or ignore it. Subscribing to a thread
is unnecessary if the user is already subscribed to the repository. Ignoring
a thread will mute all future notifications (until you comment or get @mentioned).</td>
    <td style="padding:15px">{base_path}/{version}/notifications/threads/{pathv1}/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgsOrg(org, callback)</td>
    <td style="padding:15px">Get an Organization.</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchOrgsOrg(org, body, callback)</td>
    <td style="padding:15px">Edit an Organization.</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgsOrgMembersUsername(org, username, callback)</td>
    <td style="padding:15px">Remove a member.
Removing a user from this list will remove them from all teams and they
will no longer have any access to the organization's repositories.</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgsOrgMembersUsername(org, username, callback)</td>
    <td style="padding:15px">Check if a user is, publicly or privately, a member of the organization.</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgsOrgPublicMembersUsername(org, username, callback)</td>
    <td style="padding:15px">Conceal a user's membership.</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/public_members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgsOrgPublicMembersUsername(org, username, callback)</td>
    <td style="padding:15px">Check public membership.</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/public_members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOrgsOrgPublicMembersUsername(org, username, callback)</td>
    <td style="padding:15px">Publicize a user's membership.</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/public_members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgsOrgRepos(org, body, callback)</td>
    <td style="padding:15px">Create a new repository for the authenticated user. OAuth users must supply
repo scope.</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/repos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgsOrgTeams(org, body, callback)</td>
    <td style="padding:15px">Create team.
In order to create a team, the authenticated user must be an owner of organization.</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRateLimit(callback)</td>
    <td style="padding:15px">Get your current rate limit status
Note: Accessing this endpoint does not count against your rate limit.</td>
    <td style="padding:15px">{base_path}/{version}/rate_limit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepo(owner, repo, callback)</td>
    <td style="padding:15px">Delete a Repository.
Deleting a repository requires admin access. If OAuth is used, the delete_repo
scope is required.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepo(owner, repo, callback)</td>
    <td style="padding:15px">Get repository.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepo(owner, repo, body, callback)</td>
    <td style="padding:15px">Edit repository.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoAssigneesAssignee(owner, repo, assignee, callback)</td>
    <td style="padding:15px">Check assignee.
You may also check to see if a particular user is an assignee for a repository.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/assignees/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoBranchesBranch(owner, repo, branch, callback)</td>
    <td style="padding:15px">Get Branch</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoCollaboratorsUser(owner, repo, user, callback)</td>
    <td style="padding:15px">Remove collaborator.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/collaborators/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoCollaboratorsUser(owner, repo, user, callback)</td>
    <td style="padding:15px">Check if user is a collaborator</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/collaborators/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putReposOwnerRepoCollaboratorsUser(owner, repo, user, callback)</td>
    <td style="padding:15px">Add collaborator.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/collaborators/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoCommentsCommentId(owner, repo, commentId, callback)</td>
    <td style="padding:15px">Delete a commit comment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoCommentsCommentId(owner, repo, commentId, callback)</td>
    <td style="padding:15px">Get a single commit comment.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepoCommentsCommentId(owner, repo, commentId, body, callback)</td>
    <td style="padding:15px">Update a commit comment.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoCommitsShaCode(owner, repo, shaCode, callback)</td>
    <td style="padding:15px">Get a single commit.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/commits/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoCommitsShaCodeComments(owner, repo, shaCode, body, callback)</td>
    <td style="padding:15px">Create a commit comment.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/commits/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoContentsPath(owner, repo, pathParam, body, callback)</td>
    <td style="padding:15px">Delete a file.
This method deletes a file in a repository.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/contents/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoContentsPath(owner, repo, pathParam, pathParamQuery, ref, callback)</td>
    <td style="padding:15px">Get contents.
This method returns the contents of a file or directory in a repository.
Files and symlinks support a custom media type for getting the raw content.
Directories and submodules do not support custom media types.
Note: This API supports files up to 1 megabyte in size.
Here can be many outcomes. For details see "http://developer.github.com/v3/repos/contents/"</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/contents/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putReposOwnerRepoContentsPath(owner, repo, pathParam, body, callback)</td>
    <td style="padding:15px">Create a file.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/contents/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoDeployments(owner, repo, body, callback)</td>
    <td style="padding:15px">Users with push access can create a deployment for a given ref</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoDeploymentsIdStatuses(owner, repo, id, body, callback)</td>
    <td style="padding:15px">Create a Deployment Status
Users with push access can create deployment statuses for a given deployment:</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/deployments/{pathv3}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoDownloads(owner, repo, callback)</td>
    <td style="padding:15px">Deprecated. List downloads for a repository.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/downloads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoDownloadsDownloadId(owner, repo, downloadId, callback)</td>
    <td style="padding:15px">Deprecated. Delete a download.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/downloads/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoDownloadsDownloadId(owner, repo, downloadId, callback)</td>
    <td style="padding:15px">Deprecated. Get a single download.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/downloads/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoForks(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a fork.
Forking a Repository happens asynchronously. Therefore, you may have to wai
a short period before accessing the git objects. If this takes longer than 5
minutes, be sure to contact Support.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/forks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoGitBlobs(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a Blob.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/blobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoGitBlobsShaCode(owner, repo, shaCode, callback)</td>
    <td style="padding:15px">Get a Blob.
Since blobs can be any arbitrary binary data, the input and responses for
the blob API takes an encoding parameter that can be either utf-8 or
base64. If your data cannot be losslessly sent as a UTF-8 string, you can
base64 encode it.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/blobs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoGitCommits(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a Commit.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoGitCommitsShaCode(owner, repo, shaCode, callback)</td>
    <td style="padding:15px">Get a Commit.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/commits/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoGitRefs(owner, repo, callback)</td>
    <td style="padding:15px">Get all References</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/refs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoGitRefs(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a Reference</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/refs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoGitRefsRef(owner, repo, ref, callback)</td>
    <td style="padding:15px">Delete a Reference
Example: Deleting a branch: DELETE /repos/octocat/Hello-World/git/refs/heads/feature-a
Example: Deleting a tag:        DELETE /repos/octocat/Hello-World/git/refs/tags/v1.0</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/refs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoGitMatchingRefs(owner, repo, ref, callback)</td>
    <td style="padding:15px">Get all Matching References</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/matching-refs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoGitRefsRef(owner, repo, ref, callback)</td>
    <td style="padding:15px">Get a Reference</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/refs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepoGitRefsRef(owner, repo, ref, body, callback)</td>
    <td style="padding:15px">Update a Reference</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/refs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoGitTags(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a Tag Object.
Note that creating a tag object does not create the reference that makes a
tag in Git. If you want to create an annotated tag in Git, you have to do
this call to create the tag object, and then create the refs/tags/[tag]
reference. If you want to create a lightweight tag, you only have to create
the tag reference - this call would be unnecessary.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoGitTagsShaCode(owner, repo, shaCode, callback)</td>
    <td style="padding:15px">Get a Tag.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoGitTrees(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a Tree.
The tree creation API will take nested entries as well. If both a tree and
a nested path modifying that tree are specified, it will overwrite the
contents of that tree with the new path contents and write a new tree out.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/trees?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoGitTreesShaCode(owner, repo, shaCode, recursive, callback)</td>
    <td style="padding:15px">Get a Tree.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/trees/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoHooks(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a hook.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoHooksHookId(owner, repo, hookId, callback)</td>
    <td style="padding:15px">Delete a hook.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoHooksHookId(owner, repo, hookId, callback)</td>
    <td style="padding:15px">Get single hook.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepoHooksHookId(owner, repo, hookId, body, callback)</td>
    <td style="padding:15px">Edit a hook.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoHooksHookIdTests(owner, repo, hookId, callback)</td>
    <td style="padding:15px">Test a push hook.
This will trigger the hook with the latest push to the current repository
if the hook is subscribed to push events. If the hook is not subscribed
to push events, the server will respond with 204 but no test POST will
be generated.
Note: Previously /repos/:owner/:repo/hooks/:id/tes</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks/{pathv3}/tests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoIssues(owner, repo, filter = 'assigned', state = 'open', labels, sort = 'created', direction = 'asc', since, callback)</td>
    <td style="padding:15px">List issues for a repository.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoIssues(owner, repo, body, callback)</td>
    <td style="padding:15px">Create an issue.
Any user with pull access to a repository can create an issue.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoIssuesCommentsCommentId(owner, repo, commentId, callback)</td>
    <td style="padding:15px">Delete a comment.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoIssuesCommentsCommentId(owner, repo, commentId, callback)</td>
    <td style="padding:15px">Get a single comment.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepoIssuesCommentsCommentId(owner, repo, commentId, body, callback)</td>
    <td style="padding:15px">Edit a comment.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoIssuesEventsEventId(owner, repo, eventId, callback)</td>
    <td style="padding:15px">Get a single event.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/events/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoIssuesNumber(owner, repo, number, callback)</td>
    <td style="padding:15px">Get a single issue</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepoIssuesNumber(owner, repo, number, body, callback)</td>
    <td style="padding:15px">Edit an issue.
Issue owners and users with push access can edit an issue.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoIssuesNumberComments(owner, repo, number, body, callback)</td>
    <td style="padding:15px">Create a comment.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoIssuesNumberLabels(owner, repo, number, callback)</td>
    <td style="padding:15px">Remove all labels from an issue.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoIssuesNumberLabels(owner, repo, number, body, callback)</td>
    <td style="padding:15px">Add labels to an issue.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putReposOwnerRepoIssuesNumberLabels(owner, repo, number, body, callback)</td>
    <td style="padding:15px">Replace all labels for an issue.
Sending an empty array ([]) will remove all Labels from the Issue.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoIssuesNumberLabelsName(owner, repo, number, name, callback)</td>
    <td style="padding:15px">Remove a label from an issue.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/labels/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoKeys(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a key.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoKeysKeyId(owner, repo, keyId, callback)</td>
    <td style="padding:15px">Delete a key.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoKeysKeyId(owner, repo, keyId, callback)</td>
    <td style="padding:15px">Get a key</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoLabels(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a label.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoLabelsName(owner, repo, name, callback)</td>
    <td style="padding:15px">Delete a label.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/labels/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoLabelsName(owner, repo, name, callback)</td>
    <td style="padding:15px">Get a single label.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/labels/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepoLabelsName(owner, repo, name, body, callback)</td>
    <td style="padding:15px">Update a label.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/labels/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoLanguages(owner, repo, callback)</td>
    <td style="padding:15px">List languages.
List languages for the specified repository. The value on the right of a
language is the number of bytes of code written in that language.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/languages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoMerges(owner, repo, body, callback)</td>
    <td style="padding:15px">Perform a merge.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/merges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoMilestones(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a milestone.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/milestones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoMilestonesNumber(owner, repo, number, callback)</td>
    <td style="padding:15px">Delete a milestone.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/milestones/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoMilestonesNumber(owner, repo, number, callback)</td>
    <td style="padding:15px">Get a single milestone.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/milestones/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepoMilestonesNumber(owner, repo, number, body, callback)</td>
    <td style="padding:15px">Update a milestone.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/milestones/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putReposOwnerRepoNotifications(owner, repo, body, callback)</td>
    <td style="padding:15px">Mark notifications as read in a repository.
Marking all notifications in a repository as "read" removes them from the
default view on GitHub.com.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoPulls(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoPullsCommentsCommentId(owner, repo, commentId, callback)</td>
    <td style="padding:15px">Delete a comment.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoPullsCommentsCommentId(owner, repo, commentId, callback)</td>
    <td style="padding:15px">Get a single comment.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepoPullsCommentsCommentId(owner, repo, commentId, body, callback)</td>
    <td style="padding:15px">Edit a comment.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoPullsNumber(owner, repo, number, callback)</td>
    <td style="padding:15px">Get a single pull request.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepoPullsNumber(owner, repo, number, body, callback)</td>
    <td style="padding:15px">Update a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoPullsNumberComments(owner, repo, number, body, callback)</td>
    <td style="padding:15px">Create a comment.
  #TODO Alternative input ( http://developer.github.com/v3/pulls/comments/ )
  description: |
    Alternative Input.
    Instead of passing commit_id, path, and position you can reply to an
    existing Pull Request Comment like this:

        body
           Required string
        in_reply_to
           Required number - Comment id to reply to.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoPullsNumberMerge(owner, repo, number, callback)</td>
    <td style="padding:15px">Get if a pull request has been merged.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putReposOwnerRepoPullsNumberMerge(owner, repo, number, body, callback)</td>
    <td style="padding:15px">Merge a pull request (Merge Button's)</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoReadme(owner, repo, ref, callback)</td>
    <td style="padding:15px">Get the README.
This method returns the preferred README for a repository.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/readme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoReleases(owner, repo, body, callback)</td>
    <td style="padding:15px">Create a release
Users with push access to the repository can create a release.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoReleasesAssetsId(owner, repo, id, callback)</td>
    <td style="padding:15px">Delete a release asset</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/assets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoReleasesAssetsId(owner, repo, id, callback)</td>
    <td style="padding:15px">Get a single release asset</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/assets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepoReleasesAssetsId(owner, repo, id, body, callback)</td>
    <td style="padding:15px">Edit a release asset
Users with push access to the repository can edit a release asset.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/assets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoReleasesId(owner, repo, id, callback)</td>
    <td style="padding:15px">Users with push access to the repository can delete a release.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoReleasesId(owner, repo, id, callback)</td>
    <td style="padding:15px">Get a single release</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReposOwnerRepoReleasesId(owner, repo, id, body, callback)</td>
    <td style="padding:15px">Users with push access to the repository can edit a release</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoStatsCodeFrequency(owner, repo, callback)</td>
    <td style="padding:15px">Get the number of additions and deletions per week.
Returns a weekly aggregate of the number of additions and deletions pushed
to a repository.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/stats/code_frequency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoStatsCommitActivity(owner, repo, callback)</td>
    <td style="padding:15px">Get the last year of commit activity data.
Returns the last year of commit activity grouped by week. The days array
is a group of commits per day, starting on Sunday.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/stats/commit_activity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoStatsContributors(owner, repo, callback)</td>
    <td style="padding:15px">Get contributors list with additions, deletions, and commit counts.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/stats/contributors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoStatsParticipation(owner, repo, callback)</td>
    <td style="padding:15px">Get the weekly commit count for the repo owner and everyone else.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/stats/participation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoStatsPunchCard(owner, repo, callback)</td>
    <td style="padding:15px">Get the number of commits per hour in each day.
Each array contains the day number, hour number, and number of commits
0-6 Sunday - Saturday
0-23 Hour of day
Number of commits

For example, [2, 14, 25] indicates that there were 25 total commits, during
the 2.00pm hour on Tuesdays. All times are based on the time zone of
individual commits.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/stats/punch_card?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoStatusesRef(owner, repo, ref, callback)</td>
    <td style="padding:15px">List Statuses for a specific Ref.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/statuses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposOwnerRepoStatusesRef(owner, repo, ref, body, callback)</td>
    <td style="padding:15px">Create a Status.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/statuses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposOwnerRepoSubscription(owner, repo, callback)</td>
    <td style="padding:15px">Delete a Repository Subscription.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoSubscription(owner, repo, callback)</td>
    <td style="padding:15px">Get a Repository Subscription.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putReposOwnerRepoSubscription(owner, repo, body, callback)</td>
    <td style="padding:15px">Set a Repository Subscription</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoWatchers(owner, repo, callback)</td>
    <td style="padding:15px">List Stargazers. New implementation.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/watchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoArchiveFormatPath(owner, repo, archiveFormat = 'tarball', pathParam, callback)</td>
    <td style="padding:15px">Get archive link.
This method will return a 302 to a URL to download a tarball or zipball
archive for a repository. Please make sure your HTTP framework is
configured to follow redirects or you will need to use the Location header
to make a second GET request.
Note: For private repositories, these links are temporary and expire quickly.</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositories(since, callback)</td>
    <td style="padding:15px">List all public repositories.
This provides a dump of every public repository, in the order that they
were created.
Note: Pagination is powered exclusively by the since parameter. is the
Link header to get the URL for the next page of repositories.</td>
    <td style="padding:15px">{base_path}/{version}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchCodeWithOptions(queryData, callback)</td>
    <td style="padding:15px">Search code.</td>
    <td style="padding:15px">{base_path}/{version}/search/code?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchIssuesWithOptions(queryData, callback)</td>
    <td style="padding:15px">Find issues by state and keyword. (This method returns up to 100 results per page.)</td>
    <td style="padding:15px">{base_path}/{version}/search/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsTeamId(teamId, callback)</td>
    <td style="padding:15px">Delete team.
In order to delete a team, the authenticated user must be an owner of the
org that the team is associated with.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsTeamId(teamId, callback)</td>
    <td style="padding:15px">Get team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTeamsTeamId(teamId, body, callback)</td>
    <td style="padding:15px">Edit team.
In order to edit a team, the authenticated user must be an owner of the org
that the team is associated with.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsTeamIdMembersUsername(teamId, username, callback)</td>
    <td style="padding:15px">The "Remove team member" API is deprecated and is scheduled for removal in the next major version of the API. We recommend using the Remove team membership API instead. It allows you to remove both active and pending memberships.

Remove team member.
In order to remove a user from a team, the authenticated user must have 'admin'
permissions to the team or be an owner of the org that the team is associated
with.
NOTE This does not delete the user, it just remove them from the team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsTeamIdMembersUsername(teamId, username, callback)</td>
    <td style="padding:15px">The "Get team member" API is deprecated and is scheduled for removal in the next major version of the API. We recommend using the Get team membership API instead. It allows you to get both active and pending memberships.

Get team member.
In order to get if a user is a member of a team, the authenticated user mus
be a member of the team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTeamsTeamIdMembersUsername(teamId, username, callback)</td>
    <td style="padding:15px">The API (described below) is deprecated and is scheduled for removal in the next major version of the API. We recommend using the Add team membership API instead. It allows you to invite new organization members to your teams.

Add team member.
In order to add a user to a team, the authenticated user must have 'admin'
permissions to the team or be an owner of the org that the team is associated
with.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsTeamIdMembershipsUsername(teamId, username, callback)</td>
    <td style="padding:15px">Remove team membership.
In order to remove a membership between a user and a team, the authenticated user must have 'admin' permissions to the team or be an owner of the organization that the team is associated with. NOTE: This does not delete the user, it just removes their membership from the team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/memberships/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsTeamIdMembershipsUsername(teamId, username, callback)</td>
    <td style="padding:15px">Get team membership.
In order to get a user's membership with a team, the authenticated user must be a member of the team or an owner of the team's organization.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/memberships/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTeamsTeamIdMembershipsUsername(teamId, username, callback)</td>
    <td style="padding:15px">Add team membership.
In order to add a membership between a user and a team, the authenticated user must have 'admin' permissions to the team or be an owner of the organization that the team is associated with.

If the user is already a part of the team's organization (meaning they're on at least one other team in the organization), this endpoint will add the user to the team.

If the user is completely unaffiliated with the team's organization (meaning they're on none of the organization's team...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/memberships/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsTeamIdReposOwnerRepo(teamId, owner, repo, callback)</td>
    <td style="padding:15px">In order to remove a repository from a team, the authenticated user must be an owner of the org that the team is associated with. NOTE: This does not delete the repository, it just removes it from the team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/repos/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsTeamIdReposOwnerRepo(teamId, owner, repo, callback)</td>
    <td style="padding:15px">Check if a team manages a repository</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/repos/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTeamsTeamIdReposOwnerRepo(teamId, owner, repo, callback)</td>
    <td style="padding:15px">In order to add a repository to a team, the authenticated user must be an owner of the org that the team is associated with. Also, the repository must be owned by the organization, or a direct fork of a repository owned by the organization.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/repos/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(callback)</td>
    <td style="padding:15px">Get the authenticated user.</td>
    <td style="padding:15px">{base_path}/{version}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUser(body, callback)</td>
    <td style="padding:15px">Update the authenticated user.</td>
    <td style="padding:15px">{base_path}/{version}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserEmails(body, callback)</td>
    <td style="padding:15px">Delete email address(es).
You can include a single email address or an array of addresses.</td>
    <td style="padding:15px">{base_path}/{version}/user/emails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserEmails(body, callback)</td>
    <td style="padding:15px">Add email address(es).
You can post a single email address or an array of addresses.</td>
    <td style="padding:15px">{base_path}/{version}/user/emails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserFollowingUsername(username, callback)</td>
    <td style="padding:15px">Unfollow a user.
Unfollowing a user requires the user to be logged in and authenticated with
basic auth or OAuth with the user:follow scope.</td>
    <td style="padding:15px">{base_path}/{version}/user/following/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserFollowingUsername(username, callback)</td>
    <td style="padding:15px">Check if you are following a user.</td>
    <td style="padding:15px">{base_path}/{version}/user/following/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUserFollowingUsername(username, callback)</td>
    <td style="padding:15px">Follow a user.
Following a user requires the user to be logged in and authenticated with
basic auth or OAuth with the user:follow scope.</td>
    <td style="padding:15px">{base_path}/{version}/user/following/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserKeys(body, callback)</td>
    <td style="padding:15px">Create a public key.</td>
    <td style="padding:15px">{base_path}/{version}/user/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserKeysKeyId(keyId, callback)</td>
    <td style="padding:15px">Delete a public key. Removes a public key. Requires that you are authenticated via Basic Auth or via OAuth with at least admin:public_key scope.</td>
    <td style="padding:15px">{base_path}/{version}/user/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserKeysKeyId(keyId, callback)</td>
    <td style="padding:15px">Get a single public key.</td>
    <td style="padding:15px">{base_path}/{version}/user/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserRepos(body, callback)</td>
    <td style="padding:15px">Create a new repository for the authenticated user. OAuth users must supply
repo scope.</td>
    <td style="padding:15px">{base_path}/{version}/user/repos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserStarredOwnerRepo(owner, repo, callback)</td>
    <td style="padding:15px">Unstar a repository</td>
    <td style="padding:15px">{base_path}/{version}/user/starred/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserStarredOwnerRepo(owner, repo, callback)</td>
    <td style="padding:15px">Check if you are starring a repository.</td>
    <td style="padding:15px">{base_path}/{version}/user/starred/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUserStarredOwnerRepo(owner, repo, callback)</td>
    <td style="padding:15px">Star a repository.</td>
    <td style="padding:15px">{base_path}/{version}/user/starred/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserSubscriptionsOwnerRepo(owner, repo, callback)</td>
    <td style="padding:15px">Stop watching a repository</td>
    <td style="padding:15px">{base_path}/{version}/user/subscriptions/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserSubscriptionsOwnerRepo(owner, repo, callback)</td>
    <td style="padding:15px">Check if you are watching a repository.</td>
    <td style="padding:15px">{base_path}/{version}/user/subscriptions/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUserSubscriptionsOwnerRepo(owner, repo, callback)</td>
    <td style="padding:15px">Watch a repository.</td>
    <td style="padding:15px">{base_path}/{version}/user/subscriptions/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsername(username, callback)</td>
    <td style="padding:15px">Get a single user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameFollowingTargetUser(username, targetUser, callback)</td>
    <td style="padding:15px">Check if one user follows another.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/following/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetActionsCacheUsageForOrg(org, callback)</td>
    <td style="padding:15px">actions/get-actions-cache-usage-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/cache/usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetActionsCacheUsageByRepoForOrg(org, perPage, page, callback)</td>
    <td style="padding:15px">actions/get-actions-cache-usage-by-repo-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/cache/usage-by-repository?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetGithubActionsPermissionsOrganization(org, callback)</td>
    <td style="padding:15px">actions/get-github-actions-permissions-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetGithubActionsPermissionsOrganization(org, body, callback)</td>
    <td style="padding:15px">actions/set-github-actions-permissions-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListSelectedRepositoriesEnabledGithubActionsOrganization(org, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-selected-repositories-enabled-github-actions-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/permissions/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetSelectedRepositoriesEnabledGithubActionsOrganization(org, body, callback)</td>
    <td style="padding:15px">actions/set-selected-repositories-enabled-github-actions-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/permissions/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsEnableSelectedRepositoryGithubActionsOrganization(org, repositoryId, callback)</td>
    <td style="padding:15px">actions/enable-selected-repository-github-actions-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/permissions/repositories/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDisableSelectedRepositoryGithubActionsOrganization(org, repositoryId, callback)</td>
    <td style="padding:15px">actions/disable-selected-repository-github-actions-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/permissions/repositories/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetAllowedActionsOrganization(org, callback)</td>
    <td style="padding:15px">actions/get-allowed-actions-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/permissions/selected-actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetAllowedActionsOrganization(org, body, callback)</td>
    <td style="padding:15px">actions/set-allowed-actions-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/permissions/selected-actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetGithubActionsDefaultWorkflowPermissionsOrganization(org, callback)</td>
    <td style="padding:15px">actions/get-github-actions-default-workflow-permissions-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/permissions/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetGithubActionsDefaultWorkflowPermissionsOrganization(org, body, callback)</td>
    <td style="padding:15px">actions/set-github-actions-default-workflow-permissions-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/permissions/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListSelfHostedRunnersForOrg(name, org, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-self-hosted-runners-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListRunnerApplicationsForOrg(org, callback)</td>
    <td style="padding:15px">actions/list-runner-applications-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners/downloads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGenerateRunnerJitconfigForOrg(org, body, callback)</td>
    <td style="padding:15px">actions/generate-runner-jitconfig-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners/generate-jitconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCreateRegistrationTokenForOrg(org, callback)</td>
    <td style="padding:15px">actions/create-registration-token-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners/registration-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCreateRemoveTokenForOrg(org, callback)</td>
    <td style="padding:15px">actions/create-remove-token-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners/remove-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetSelfHostedRunnerForOrg(org, runnerId, callback)</td>
    <td style="padding:15px">actions/get-self-hosted-runner-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteSelfHostedRunnerFromOrg(org, runnerId, callback)</td>
    <td style="padding:15px">actions/delete-self-hosted-runner-from-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListLabelsForSelfHostedRunnerForOrg(org, runnerId, callback)</td>
    <td style="padding:15px">actions/list-labels-for-self-hosted-runner-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners/{pathv2}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsAddCustomLabelsToSelfHostedRunnerForOrg(org, runnerId, body, callback)</td>
    <td style="padding:15px">actions/add-custom-labels-to-self-hosted-runner-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners/{pathv2}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetCustomLabelsForSelfHostedRunnerForOrg(org, runnerId, body, callback)</td>
    <td style="padding:15px">actions/set-custom-labels-for-self-hosted-runner-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners/{pathv2}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsRemoveAllCustomLabelsFromSelfHostedRunnerForOrg(org, runnerId, callback)</td>
    <td style="padding:15px">actions/remove-all-custom-labels-from-self-hosted-runner-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners/{pathv2}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsRemoveCustomLabelFromSelfHostedRunnerForOrg(org, runnerId, name, callback)</td>
    <td style="padding:15px">actions/remove-custom-label-from-self-hosted-runner-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/runners/{pathv2}/labels/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListOrgSecrets(org, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-org-secrets</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetOrgPublicKey(org, callback)</td>
    <td style="padding:15px">actions/get-org-public-key</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/secrets/public-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetOrgSecret(org, secretName, callback)</td>
    <td style="padding:15px">actions/get-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCreateOrUpdateOrgSecret(org, secretName, body, callback)</td>
    <td style="padding:15px">actions/create-or-update-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteOrgSecret(org, secretName, callback)</td>
    <td style="padding:15px">actions/delete-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListSelectedReposForOrgSecret(org, secretName, page, perPage, callback)</td>
    <td style="padding:15px">actions/list-selected-repos-for-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/secrets/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetSelectedReposForOrgSecret(org, secretName, body, callback)</td>
    <td style="padding:15px">actions/set-selected-repos-for-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/secrets/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsAddSelectedRepoToOrgSecret(org, secretName, repositoryId, callback)</td>
    <td style="padding:15px">actions/add-selected-repo-to-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/secrets/{pathv2}/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsRemoveSelectedRepoFromOrgSecret(org, secretName, repositoryId, callback)</td>
    <td style="padding:15px">actions/remove-selected-repo-from-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/secrets/{pathv2}/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListOrgVariables(org, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-org-variables</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCreateOrgVariable(org, body, callback)</td>
    <td style="padding:15px">actions/create-org-variable</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetOrgVariable(org, name, callback)</td>
    <td style="padding:15px">actions/get-org-variable</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsUpdateOrgVariable(org, name, body, callback)</td>
    <td style="padding:15px">actions/update-org-variable</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteOrgVariable(org, name, callback)</td>
    <td style="padding:15px">actions/delete-org-variable</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListSelectedReposForOrgVariable(org, name, page, perPage, callback)</td>
    <td style="padding:15px">actions/list-selected-repos-for-org-variable</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/variables/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetSelectedReposForOrgVariable(org, name, body, callback)</td>
    <td style="padding:15px">actions/set-selected-repos-for-org-variable</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/variables/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsAddSelectedRepoToOrgVariable(org, name, repositoryId, callback)</td>
    <td style="padding:15px">actions/add-selected-repo-to-org-variable</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/variables/{pathv2}/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsRemoveSelectedRepoFromOrgVariable(org, name, repositoryId, callback)</td>
    <td style="padding:15px">actions/remove-selected-repo-from-org-variable</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/variables/{pathv2}/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListArtifactsForRepo(owner, repo, perPage, page, name, callback)</td>
    <td style="padding:15px">actions/list-artifacts-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/artifacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetArtifact(owner, repo, artifactId, callback)</td>
    <td style="padding:15px">actions/get-artifact</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/artifacts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteArtifact(owner, repo, artifactId, callback)</td>
    <td style="padding:15px">actions/delete-artifact</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/artifacts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDownloadArtifact(owner, repo, artifactId, archiveFormat, callback)</td>
    <td style="padding:15px">actions/download-artifact</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/artifacts/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetActionsCacheUsage(owner, repo, callback)</td>
    <td style="padding:15px">actions/get-actions-cache-usage</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/cache/usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetActionsCacheList(owner, repo, perPage, page, ref, key, sort, direction, callback)</td>
    <td style="padding:15px">actions/get-actions-cache-list</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/caches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteActionsCacheByKey(owner, repo, key, ref, callback)</td>
    <td style="padding:15px">actions/delete-actions-cache-by-key</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/caches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteActionsCacheById(owner, repo, cacheId, callback)</td>
    <td style="padding:15px">actions/delete-actions-cache-by-id</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/caches/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetJobForWorkflowRun(owner, repo, jobId, callback)</td>
    <td style="padding:15px">actions/get-job-for-workflow-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/jobs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDownloadJobLogsForWorkflowRun(owner, repo, jobId, callback)</td>
    <td style="padding:15px">actions/download-job-logs-for-workflow-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/jobs/{pathv3}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsReRunJobForWorkflowRun(owner, repo, jobId, body, callback)</td>
    <td style="padding:15px">actions/re-run-job-for-workflow-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/jobs/{pathv3}/rerun?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetCustomOidcSubClaimForRepo(owner, repo, callback)</td>
    <td style="padding:15px">actions/get-custom-oidc-sub-claim-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/oidc/customization/sub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetCustomOidcSubClaimForRepo(owner, repo, body, callback)</td>
    <td style="padding:15px">actions/set-custom-oidc-sub-claim-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/oidc/customization/sub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListRepoOrganizationSecrets(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-repo-organization-secrets</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/organization-secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListRepoOrganizationVariables(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-repo-organization-variables</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/organization-variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetGithubActionsPermissionsRepository(owner, repo, callback)</td>
    <td style="padding:15px">actions/get-github-actions-permissions-repository</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetGithubActionsPermissionsRepository(owner, repo, body, callback)</td>
    <td style="padding:15px">actions/set-github-actions-permissions-repository</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetWorkflowAccessToRepository(owner, repo, callback)</td>
    <td style="padding:15px">actions/get-workflow-access-to-repository</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/permissions/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetWorkflowAccessToRepository(owner, repo, body, callback)</td>
    <td style="padding:15px">actions/set-workflow-access-to-repository</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/permissions/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetAllowedActionsRepository(owner, repo, callback)</td>
    <td style="padding:15px">actions/get-allowed-actions-repository</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/permissions/selected-actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetAllowedActionsRepository(owner, repo, body, callback)</td>
    <td style="padding:15px">actions/set-allowed-actions-repository</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/permissions/selected-actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetGithubActionsDefaultWorkflowPermissionsRepository(owner, repo, callback)</td>
    <td style="padding:15px">actions/get-github-actions-default-workflow-permissions-repository</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/permissions/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetGithubActionsDefaultWorkflowPermissionsRepository(owner, repo, body, callback)</td>
    <td style="padding:15px">actions/set-github-actions-default-workflow-permissions-repository</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/permissions/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListSelfHostedRunnersForRepo(name, owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-self-hosted-runners-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListRunnerApplicationsForRepo(owner, repo, callback)</td>
    <td style="padding:15px">actions/list-runner-applications-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners/downloads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGenerateRunnerJitconfigForRepo(owner, repo, body, callback)</td>
    <td style="padding:15px">actions/generate-runner-jitconfig-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners/generate-jitconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCreateRegistrationTokenForRepo(owner, repo, callback)</td>
    <td style="padding:15px">actions/create-registration-token-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners/registration-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCreateRemoveTokenForRepo(owner, repo, callback)</td>
    <td style="padding:15px">actions/create-remove-token-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners/remove-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetSelfHostedRunnerForRepo(owner, repo, runnerId, callback)</td>
    <td style="padding:15px">actions/get-self-hosted-runner-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteSelfHostedRunnerFromRepo(owner, repo, runnerId, callback)</td>
    <td style="padding:15px">actions/delete-self-hosted-runner-from-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListLabelsForSelfHostedRunnerForRepo(owner, repo, runnerId, callback)</td>
    <td style="padding:15px">actions/list-labels-for-self-hosted-runner-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners/{pathv3}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsAddCustomLabelsToSelfHostedRunnerForRepo(owner, repo, runnerId, body, callback)</td>
    <td style="padding:15px">actions/add-custom-labels-to-self-hosted-runner-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners/{pathv3}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsSetCustomLabelsForSelfHostedRunnerForRepo(owner, repo, runnerId, body, callback)</td>
    <td style="padding:15px">actions/set-custom-labels-for-self-hosted-runner-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners/{pathv3}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsRemoveAllCustomLabelsFromSelfHostedRunnerForRepo(owner, repo, runnerId, callback)</td>
    <td style="padding:15px">actions/remove-all-custom-labels-from-self-hosted-runner-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners/{pathv3}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsRemoveCustomLabelFromSelfHostedRunnerForRepo(owner, repo, runnerId, name, callback)</td>
    <td style="padding:15px">actions/remove-custom-label-from-self-hosted-runner-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runners/{pathv3}/labels/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListWorkflowRunsForRepo(owner, repo, actor, branch, event, status, perPage, page, created, excludePullRequests, checkSuiteId, headSha, callback)</td>
    <td style="padding:15px">actions/list-workflow-runs-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetWorkflowRun(owner, repo, runId, excludePullRequests, callback)</td>
    <td style="padding:15px">actions/get-workflow-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteWorkflowRun(owner, repo, runId, callback)</td>
    <td style="padding:15px">actions/delete-workflow-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetReviewsForRun(owner, repo, runId, callback)</td>
    <td style="padding:15px">actions/get-reviews-for-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/approvals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsApproveWorkflowRun(owner, repo, runId, callback)</td>
    <td style="padding:15px">actions/approve-workflow-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListWorkflowRunArtifacts(owner, repo, runId, perPage, page, name, callback)</td>
    <td style="padding:15px">actions/list-workflow-run-artifacts</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/artifacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetWorkflowRunAttempt(owner, repo, runId, attemptNumber, excludePullRequests, callback)</td>
    <td style="padding:15px">actions/get-workflow-run-attempt</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/attempts/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListJobsForWorkflowRunAttempt(owner, repo, runId, attemptNumber, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-jobs-for-workflow-run-attempt</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/attempts/{pathv4}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDownloadWorkflowRunAttemptLogs(owner, repo, runId, attemptNumber, callback)</td>
    <td style="padding:15px">actions/download-workflow-run-attempt-logs</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/attempts/{pathv4}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCancelWorkflowRun(owner, repo, runId, callback)</td>
    <td style="padding:15px">actions/cancel-workflow-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsReviewCustomGatesForRun(owner, repo, runId, body, callback)</td>
    <td style="padding:15px">actions/review-custom-gates-for-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/deployment_protection_rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsForceCancelWorkflowRun(owner, repo, runId, callback)</td>
    <td style="padding:15px">actions/force-cancel-workflow-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/force-cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListJobsForWorkflowRun(owner, repo, runId, filter, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-jobs-for-workflow-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDownloadWorkflowRunLogs(owner, repo, runId, callback)</td>
    <td style="padding:15px">actions/download-workflow-run-logs</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteWorkflowRunLogs(owner, repo, runId, callback)</td>
    <td style="padding:15px">actions/delete-workflow-run-logs</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetPendingDeploymentsForRun(owner, repo, runId, callback)</td>
    <td style="padding:15px">actions/get-pending-deployments-for-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/pending_deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsReviewPendingDeploymentsForRun(owner, repo, runId, body, callback)</td>
    <td style="padding:15px">actions/review-pending-deployments-for-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/pending_deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsReRunWorkflow(owner, repo, runId, body, callback)</td>
    <td style="padding:15px">actions/re-run-workflow</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/rerun?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsReRunWorkflowFailedJobs(owner, repo, runId, body, callback)</td>
    <td style="padding:15px">actions/re-run-workflow-failed-jobs</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/rerun-failed-jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetWorkflowRunUsage(owner, repo, runId, callback)</td>
    <td style="padding:15px">actions/get-workflow-run-usage</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/runs/{pathv3}/timing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListRepoSecrets(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-repo-secrets</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetRepoPublicKey(owner, repo, callback)</td>
    <td style="padding:15px">actions/get-repo-public-key</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/secrets/public-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetRepoSecret(owner, repo, secretName, callback)</td>
    <td style="padding:15px">actions/get-repo-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/secrets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCreateOrUpdateRepoSecret(owner, repo, secretName, body, callback)</td>
    <td style="padding:15px">actions/create-or-update-repo-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/secrets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteRepoSecret(owner, repo, secretName, callback)</td>
    <td style="padding:15px">actions/delete-repo-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/secrets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListRepoVariables(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-repo-variables</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCreateRepoVariable(owner, repo, body, callback)</td>
    <td style="padding:15px">actions/create-repo-variable</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetRepoVariable(owner, repo, name, callback)</td>
    <td style="padding:15px">actions/get-repo-variable</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/variables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsUpdateRepoVariable(owner, repo, name, body, callback)</td>
    <td style="padding:15px">actions/update-repo-variable</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/variables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteRepoVariable(owner, repo, name, callback)</td>
    <td style="padding:15px">actions/delete-repo-variable</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/variables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListRepoWorkflows(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-repo-workflows</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/workflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetWorkflow(owner, repo, workflowId, callback)</td>
    <td style="padding:15px">actions/get-workflow</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/workflows/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDisableWorkflow(owner, repo, workflowId, callback)</td>
    <td style="padding:15px">actions/disable-workflow</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/workflows/{pathv3}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCreateWorkflowDispatch(owner, repo, workflowId, body, callback)</td>
    <td style="padding:15px">actions/create-workflow-dispatch</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/workflows/{pathv3}/dispatches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsEnableWorkflow(owner, repo, workflowId, callback)</td>
    <td style="padding:15px">actions/enable-workflow</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/workflows/{pathv3}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListWorkflowRuns(owner, repo, workflowId, actor, branch, event, status, perPage, page, created, excludePullRequests, checkSuiteId, headSha, callback)</td>
    <td style="padding:15px">actions/list-workflow-runs</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/workflows/{pathv3}/runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetWorkflowUsage(owner, repo, workflowId, callback)</td>
    <td style="padding:15px">actions/get-workflow-usage</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/actions/workflows/{pathv3}/timing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListEnvironmentSecrets(owner, repo, environmentName, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-environment-secrets</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetEnvironmentPublicKey(owner, repo, environmentName, callback)</td>
    <td style="padding:15px">actions/get-environment-public-key</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/secrets/public-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetEnvironmentSecret(owner, repo, environmentName, secretName, callback)</td>
    <td style="padding:15px">actions/get-environment-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/secrets/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCreateOrUpdateEnvironmentSecret(owner, repo, environmentName, secretName, body, callback)</td>
    <td style="padding:15px">actions/create-or-update-environment-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/secrets/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteEnvironmentSecret(owner, repo, environmentName, secretName, callback)</td>
    <td style="padding:15px">actions/delete-environment-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/secrets/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsListEnvironmentVariables(owner, repo, environmentName, perPage, page, callback)</td>
    <td style="padding:15px">actions/list-environment-variables</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsCreateEnvironmentVariable(owner, repo, environmentName, body, callback)</td>
    <td style="padding:15px">actions/create-environment-variable</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsGetEnvironmentVariable(owner, repo, environmentName, name, callback)</td>
    <td style="padding:15px">actions/get-environment-variable</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/variables/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsUpdateEnvironmentVariable(owner, repo, name, environmentName, body, callback)</td>
    <td style="padding:15px">actions/update-environment-variable</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/variables/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionsDeleteEnvironmentVariable(owner, repo, name, environmentName, callback)</td>
    <td style="padding:15px">actions/delete-environment-variable</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/variables/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventsQuery(perPage, page, callback)</td>
    <td style="padding:15px">activity/list-public-events</td>
    <td style="padding:15px">{base_path}/{version}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworksOwnerRepoEventsQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-public-events-for-repo-network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/{pathv2}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotificationsQuery(all, participating, since, before, page, perPage, callback)</td>
    <td style="padding:15px">activity/list-notifications-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activityGetThread(id, callback)</td>
    <td style="padding:15px">activity/get-thread</td>
    <td style="padding:15px">{base_path}/{version}/notifications/threads/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activityMarkThreadAsRead(id, callback)</td>
    <td style="padding:15px">activity/mark-thread-as-read</td>
    <td style="padding:15px">{base_path}/{version}/notifications/threads/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activityMarkThreadAsDone(id, callback)</td>
    <td style="padding:15px">activity/mark-thread-as-done</td>
    <td style="padding:15px">{base_path}/{version}/notifications/threads/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activityGetThreadSubscriptionForAuthenticatedUser(id, callback)</td>
    <td style="padding:15px">activity/get-thread-subscription-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/notifications/threads/{pathv1}/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activitySetThreadSubscription(id, body, callback)</td>
    <td style="padding:15px">activity/set-thread-subscription</td>
    <td style="padding:15px">{base_path}/{version}/notifications/threads/{pathv1}/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activityDeleteThreadSubscription(id, callback)</td>
    <td style="padding:15px">activity/delete-thread-subscription</td>
    <td style="padding:15px">{base_path}/{version}/notifications/threads/{pathv1}/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activityListPublicEventsForUser(username, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-public-events-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/events/public?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgsOrgEventsQuery(org, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-public-org-events</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgsOrgIssuesQuery(org, filter, state, labels, sort, direction, since, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsList(since, perPage, callback)</td>
    <td style="padding:15px">orgs/list</td>
    <td style="padding:15px">{base_path}/{version}/organizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsDelete(org, callback)</td>
    <td style="padding:15px">orgs/delete</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListAttestations(perPage, before, after, org, subjectDigest, callback)</td>
    <td style="padding:15px">orgs/list-attestations</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/attestations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListBlockedUsers(org, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-blocked-users</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/blocks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsCheckBlockedUser(org, username, callback)</td>
    <td style="padding:15px">orgs/check-blocked-user</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/blocks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsBlockUser(org, username, callback)</td>
    <td style="padding:15px">orgs/block-user</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/blocks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsUnblockUser(org, username, callback)</td>
    <td style="padding:15px">orgs/unblock-user</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/blocks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListFailedInvitations(org, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-failed-invitations</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/failed_invitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListWebhooks(org, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-webhooks</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsCreateWebhook(org, body, callback)</td>
    <td style="padding:15px">orgs/create-webhook</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsGetWebhook(org, hookId, callback)</td>
    <td style="padding:15px">orgs/get-webhook</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsUpdateWebhook(org, hookId, body, callback)</td>
    <td style="padding:15px">orgs/update-webhook</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsDeleteWebhook(org, hookId, callback)</td>
    <td style="padding:15px">orgs/delete-webhook</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsGetWebhookConfigForOrg(org, hookId, callback)</td>
    <td style="padding:15px">orgs/get-webhook-config-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/hooks/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsUpdateWebhookConfigForOrg(org, hookId, body, callback)</td>
    <td style="padding:15px">orgs/update-webhook-config-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/hooks/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListWebhookDeliveries(org, hookId, perPage, cursor, redelivery, callback)</td>
    <td style="padding:15px">orgs/list-webhook-deliveries</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/hooks/{pathv2}/deliveries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsGetWebhookDelivery(org, hookId, deliveryId, callback)</td>
    <td style="padding:15px">orgs/get-webhook-delivery</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/hooks/{pathv2}/deliveries/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsRedeliverWebhookDelivery(org, hookId, deliveryId, callback)</td>
    <td style="padding:15px">orgs/redeliver-webhook-delivery</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/hooks/{pathv2}/deliveries/{pathv3}/attempts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsPingWebhook(org, hookId, callback)</td>
    <td style="padding:15px">orgs/ping-webhook</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/hooks/{pathv2}/pings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListAppInstallations(org, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-app-installations</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/installations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListPendingInvitations(org, perPage, page, role, invitationSource, callback)</td>
    <td style="padding:15px">orgs/list-pending-invitations</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/invitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsCreateInvitation(org, body, callback)</td>
    <td style="padding:15px">orgs/create-invitation</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/invitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsCancelInvitation(org, invitationId, callback)</td>
    <td style="padding:15px">orgs/cancel-invitation</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/invitations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListInvitationTeams(org, invitationId, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-invitation-teams</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/invitations/{pathv2}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgsOrgMembersQuery(org, filter, role, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-members</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsGetMembershipForUser(org, username, callback)</td>
    <td style="padding:15px">orgs/get-membership-for-user</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/memberships/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsSetMembershipForUser(org, username, body, callback)</td>
    <td style="padding:15px">orgs/set-membership-for-user</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/memberships/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsRemoveMembershipForUser(org, username, callback)</td>
    <td style="padding:15px">orgs/remove-membership-for-user</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/memberships/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListOrgRoles(org, callback)</td>
    <td style="padding:15px">orgs/list-org-roles</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/organization-roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsRevokeAllOrgRolesTeam(org, teamSlug, callback)</td>
    <td style="padding:15px">orgs/revoke-all-org-roles-team</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/organization-roles/teams/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsAssignTeamToOrgRole(org, teamSlug, roleId, callback)</td>
    <td style="padding:15px">orgs/assign-team-to-org-role</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/organization-roles/teams/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsRevokeOrgRoleTeam(org, teamSlug, roleId, callback)</td>
    <td style="padding:15px">orgs/revoke-org-role-team</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/organization-roles/teams/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsRevokeAllOrgRolesUser(org, username, callback)</td>
    <td style="padding:15px">orgs/revoke-all-org-roles-user</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/organization-roles/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsAssignUserToOrgRole(org, username, roleId, callback)</td>
    <td style="padding:15px">orgs/assign-user-to-org-role</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/organization-roles/users/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsRevokeOrgRoleUser(org, username, roleId, callback)</td>
    <td style="padding:15px">orgs/revoke-org-role-user</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/organization-roles/users/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsGetOrgRole(org, roleId, callback)</td>
    <td style="padding:15px">orgs/get-org-role</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/organization-roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListOrgRoleTeams(org, roleId, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-org-role-teams</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/organization-roles/{pathv2}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListOrgRoleUsers(org, roleId, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-org-role-users</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/organization-roles/{pathv2}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListOutsideCollaborators(org, filter, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-outside-collaborators</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/outside_collaborators?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsConvertMemberToOutsideCollaborator(org, username, body, callback)</td>
    <td style="padding:15px">orgs/convert-member-to-outside-collaborator</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/outside_collaborators/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsRemoveOutsideCollaborator(org, username, callback)</td>
    <td style="padding:15px">orgs/remove-outside-collaborator</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/outside_collaborators/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListPatGrantRequests(org, perPage, page, sort, direction, owner, repository, permission, lastUsedBefore, lastUsedAfter, callback)</td>
    <td style="padding:15px">orgs/list-pat-grant-requests</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/personal-access-token-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsReviewPatGrantRequestsInBulk(org, body, callback)</td>
    <td style="padding:15px">orgs/review-pat-grant-requests-in-bulk</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/personal-access-token-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsReviewPatGrantRequest(org, patRequestId, body, callback)</td>
    <td style="padding:15px">orgs/review-pat-grant-request</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/personal-access-token-requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListPatGrantRequestRepositories(org, patRequestId, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-pat-grant-request-repositories</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/personal-access-token-requests/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListPatGrants(org, perPage, page, sort, direction, owner, repository, permission, lastUsedBefore, lastUsedAfter, callback)</td>
    <td style="padding:15px">orgs/list-pat-grants</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/personal-access-tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsUpdatePatAccesses(org, body, callback)</td>
    <td style="padding:15px">orgs/update-pat-accesses</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/personal-access-tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsUpdatePatAccess(org, patId, body, callback)</td>
    <td style="padding:15px">orgs/update-pat-access</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/personal-access-tokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListPatGrantRepositories(org, patId, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-pat-grant-repositories</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/personal-access-tokens/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsGetAllCustomProperties(org, callback)</td>
    <td style="padding:15px">orgs/get-all-custom-properties</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/properties/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsCreateOrUpdateCustomProperties(org, body, callback)</td>
    <td style="padding:15px">orgs/create-or-update-custom-properties</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/properties/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsGetCustomProperty(org, customPropertyName, callback)</td>
    <td style="padding:15px">orgs/get-custom-property</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/properties/schema/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsCreateOrUpdateCustomProperty(org, customPropertyName, body, callback)</td>
    <td style="padding:15px">orgs/create-or-update-custom-property</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/properties/schema/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsRemoveCustomProperty(org, customPropertyName, callback)</td>
    <td style="padding:15px">orgs/remove-custom-property</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/properties/schema/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListCustomPropertiesValuesForRepos(org, perPage, page, repositoryQuery, callback)</td>
    <td style="padding:15px">orgs/list-custom-properties-values-for-repos</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/properties/values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsCreateOrUpdateCustomPropertiesValuesForRepos(org, body, callback)</td>
    <td style="padding:15px">orgs/create-or-update-custom-properties-values-for-repos</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/properties/values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgsOrgPublicMembersQuery(org, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-public-members</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/public_members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListSecurityManagerTeams(org, callback)</td>
    <td style="padding:15px">orgs/list-security-manager-teams</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/security-managers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsAddSecurityManagerTeam(org, teamSlug, callback)</td>
    <td style="padding:15px">orgs/add-security-manager-team</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/security-managers/teams/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsRemoveSecurityManagerTeam(org, teamSlug, callback)</td>
    <td style="padding:15px">orgs/remove-security-manager-team</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/security-managers/teams/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsListMembershipsForAuthenticatedUser(state, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-memberships-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/memberships/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsGetMembershipForAuthenticatedUser(org, callback)</td>
    <td style="padding:15px">orgs/get-membership-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/memberships/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgsUpdateMembershipForAuthenticatedUser(org, body, callback)</td>
    <td style="padding:15px">orgs/update-membership-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/memberships/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgsOrgReposQuery(org, type, sort, direction, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/repos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgsOrgTeamsQuery(org, perPage, page, callback)</td>
    <td style="padding:15px">teams/list</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoEventsQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-repo-events</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoNotificationsQuery(owner, repo, all, participating, since, before, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-repo-notifications-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoStargazersQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-stargazers-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/stargazers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoSubscribersQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-watchers-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/subscribers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoAssigneesQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-assignees</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/assignees?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoIssuesQuery(owner, repo, milestone, state, assignee, creator, mentioned, labels, sort, direction, since, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoIssuesCommentsQuery(owner, repo, sort, direction, since, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-comments-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoIssuesEventsQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-events-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoIssuesNumberCommentsQuery(owner, repo, issueNumber, since, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-comments</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoIssuesNumberEventsQuery(owner, repo, issueNumber, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-events</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoIssuesNumberLabelsQuery(owner, repo, issueNumber, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-labels-on-issue</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoLabelsQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-labels-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoMilestonesQuery(owner, repo, state, sort, direction, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-milestones</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/milestones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoMilestonesNumberLabelsQuery(owner, repo, milestoneNumber, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-labels-for-milestone</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/milestones/{pathv3}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoPullsQuery(owner, repo, state, head, base, sort, direction, perPage, page, callback)</td>
    <td style="padding:15px">pulls/list</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoPullsCommentsQuery(owner, repo, sort = 'created', direction, since, perPage, page, callback)</td>
    <td style="padding:15px">pulls/list-review-comments-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoPullsNumberCommentsQuery(owner, repo, pullNumber, sort, direction, since, perPage, page, callback)</td>
    <td style="padding:15px">pulls/list-review-comments</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoPullsNumberCommitsQuery(owner, repo, pullNumber, perPage, page, callback)</td>
    <td style="padding:15px">pulls/list-commits</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoPullsNumberFilesQuery(owner, repo, pullNumber, perPage, page, callback)</td>
    <td style="padding:15px">pulls/list-files</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetOrgRulesets(org, perPage, page, callback)</td>
    <td style="padding:15px">repos/get-org-rulesets</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/rulesets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateOrgRuleset(org, body, callback)</td>
    <td style="padding:15px">repos/create-org-ruleset</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/rulesets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetOrgRuleSuites(org, ref, repositoryName, timePeriod, actorName, ruleSuiteResult, perPage, page, callback)</td>
    <td style="padding:15px">repos/get-org-rule-suites</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/rulesets/rule-suites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetOrgRuleSuite(org, ruleSuiteId, callback)</td>
    <td style="padding:15px">repos/get-org-rule-suite</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/rulesets/rule-suites/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetOrgRuleset(org, rulesetId, callback)</td>
    <td style="padding:15px">repos/get-org-ruleset</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/rulesets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposUpdateOrgRuleset(org, rulesetId, body, callback)</td>
    <td style="padding:15px">repos/update-org-ruleset</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/rulesets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteOrgRuleset(org, rulesetId, callback)</td>
    <td style="padding:15px">repos/delete-org-ruleset</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/rulesets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListActivities(owner, repo, direction, perPage, before, after, ref, actor, timePeriod, activityType, callback)</td>
    <td style="padding:15px">repos/list-activities</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/activity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateAttestation(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/create-attestation</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/attestations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListAttestations(owner, repo, perPage, before, after, subjectDigest, callback)</td>
    <td style="padding:15px">repos/list-attestations</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/attestations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListAutolinks(owner, repo, callback)</td>
    <td style="padding:15px">repos/list-autolinks</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/autolinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateAutolink(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/create-autolink</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/autolinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetAutolink(owner, repo, autolinkId, callback)</td>
    <td style="padding:15px">repos/get-autolink</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/autolinks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteAutolink(owner, repo, autolinkId, callback)</td>
    <td style="padding:15px">repos/delete-autolink</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/autolinks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCheckAutomatedSecurityFixes(owner, repo, callback)</td>
    <td style="padding:15px">repos/check-automated-security-fixes</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/automated-security-fixes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposEnableAutomatedSecurityFixes(owner, repo, callback)</td>
    <td style="padding:15px">repos/enable-automated-security-fixes</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/automated-security-fixes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDisableAutomatedSecurityFixes(owner, repo, callback)</td>
    <td style="padding:15px">repos/disable-automated-security-fixes</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/automated-security-fixes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoBranchesQuery(owner, repo, protectedParam, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-branches</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetBranchProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/get-branch-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposUpdateBranchProtection(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/update-branch-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteBranchProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/delete-branch-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetAdminBranchProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/get-admin-branch-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/enforce_admins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposSetAdminBranchProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/set-admin-branch-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/enforce_admins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteAdminBranchProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/delete-admin-branch-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/enforce_admins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetPullRequestReviewProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/get-pull-request-review-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_pull_request_reviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposUpdatePullRequestReviewProtection(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/update-pull-request-review-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_pull_request_reviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeletePullRequestReviewProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/delete-pull-request-review-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_pull_request_reviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetCommitSignatureProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/get-commit-signature-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_signatures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateCommitSignatureProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/create-commit-signature-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_signatures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteCommitSignatureProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/delete-commit-signature-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_signatures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetStatusChecksProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/get-status-checks-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_status_checks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposUpdateStatusCheckProtection(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/update-status-check-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_status_checks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposRemoveStatusCheckProtection(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/remove-status-check-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_status_checks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetAllStatusCheckContexts(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/get-all-status-check-contexts</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_status_checks/contexts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposAddStatusCheckContexts(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/add-status-check-contexts</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_status_checks/contexts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposSetStatusCheckContexts(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/set-status-check-contexts</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_status_checks/contexts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposRemoveStatusCheckContexts(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/remove-status-check-contexts</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/required_status_checks/contexts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetAccessRestrictions(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/get-access-restrictions</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteAccessRestrictions(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/delete-access-restrictions</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetAppsWithAccessToProtectedBranch(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/get-apps-with-access-to-protected-branch</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposAddAppAccessRestrictions(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/add-app-access-restrictions</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposSetAppAccessRestrictions(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/set-app-access-restrictions</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposRemoveAppAccessRestrictions(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/remove-app-access-restrictions</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetTeamsWithAccessToProtectedBranch(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/get-teams-with-access-to-protected-branch</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposAddTeamAccessRestrictions(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/add-team-access-restrictions</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposSetTeamAccessRestrictions(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/set-team-access-restrictions</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposRemoveTeamAccessRestrictions(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/remove-team-access-restrictions</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetUsersWithAccessToProtectedBranch(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/get-users-with-access-to-protected-branch</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposAddUserAccessRestrictions(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/add-user-access-restrictions</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposSetUserAccessRestrictions(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/set-user-access-restrictions</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposRemoveUserAccessRestrictions(owner, repo, branch, callback)</td>
    <td style="padding:15px">repos/remove-user-access-restrictions</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/protection/restrictions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposRenameBranch(owner, repo, branch, body, callback)</td>
    <td style="padding:15px">repos/rename-branch</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/branches/{pathv3}/rename?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCodeownersErrors(owner, repo, ref, callback)</td>
    <td style="padding:15px">repos/codeowners-errors</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codeowners/errors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoCollaboratorsQuery(owner, repo, affiliation, permission, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-collaborators</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/collaborators?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCheckCollaborator(owner, repo, username, callback)</td>
    <td style="padding:15px">repos/check-collaborator</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/collaborators/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposAddCollaborator(owner, repo, username, body, callback)</td>
    <td style="padding:15px">repos/add-collaborator</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/collaborators/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposRemoveCollaborator(owner, repo, username, callback)</td>
    <td style="padding:15px">repos/remove-collaborator</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/collaborators/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetCollaboratorPermissionLevel(owner, repo, username, callback)</td>
    <td style="padding:15px">repos/get-collaborator-permission-level</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/collaborators/{pathv3}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoCommentsQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-commit-comments-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoCommitsQuery(owner, repo, sha, pathParam, author, committer, since, until, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-commits</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListBranchesForHeadCommit(owner, repo, commitSha, callback)</td>
    <td style="padding:15px">repos/list-branches-for-head-commit</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/commits/{pathv3}/branches-where-head?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoCommitsShaCodeCommentsQuery(owner, repo, shaCode, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-comments-for-commit</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/commits/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListPullRequestsAssociatedWithCommit(owner, repo, shaCode, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-pull-requests-associated-with-commit</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/commits/{pathv3}/pulls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetCommit(owner, repo, page, perPage, ref, callback)</td>
    <td style="padding:15px">repos/get-commit</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/commits/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoCommitsRefStatusQuery(owner, repo, ref, perPage, page, callback)</td>
    <td style="padding:15px">repos/get-combined-status-for-ref</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/commits/{pathv3}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListCommitStatusesForRef(owner, repo, ref, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-commit-statuses-for-ref</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/commits/{pathv3}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetCommunityProfileMetrics(owner, repo, callback)</td>
    <td style="padding:15px">repos/get-community-profile-metrics</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/community/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoCompareBaseIdHeadIdQuery(owner, repo, page, perPage, baseId, headId, callback)</td>
    <td style="padding:15px">repos/compare-commits</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/compare/{pathv3}...{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetContent(owner, repo, pathParam, ref, callback)</td>
    <td style="padding:15px">repos/get-content</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/contents/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateOrUpdateFileContents(owner, repo, pathParam, body, callback)</td>
    <td style="padding:15px">repos/create-or-update-file-contents</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/contents/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteFile(owner, repo, pathParam, callback)</td>
    <td style="padding:15px">repos/delete-file</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/contents/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoContributorsQuery(owner, repo, anon, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-contributors</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/contributors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoDeploymentsQuery(owner, repo, sha, ref, task, environment, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-deployments</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetDeployment(owner, repo, deploymentId, callback)</td>
    <td style="padding:15px">repos/get-deployment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/deployments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteDeployment(owner, repo, deploymentId, callback)</td>
    <td style="padding:15px">repos/delete-deployment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/deployments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoDeploymentsIdStatusesQuery(owner, repo, deploymentId, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-deployment-statuses</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/deployments/{pathv3}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetDeploymentStatus(owner, repo, deploymentId, statusId, callback)</td>
    <td style="padding:15px">repos/get-deployment-status</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/deployments/{pathv3}/statuses/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateDispatchEvent(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/create-dispatch-event</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dispatches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetAllEnvironments(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">repos/get-all-environments</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetEnvironment(owner, repo, environmentName, callback)</td>
    <td style="padding:15px">repos/get-environment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateOrUpdateEnvironment(owner, repo, environmentName, body, callback)</td>
    <td style="padding:15px">repos/create-or-update-environment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteAnEnvironment(owner, repo, environmentName, callback)</td>
    <td style="padding:15px">repos/delete-an-environment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListDeploymentBranchPolicies(owner, repo, environmentName, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-deployment-branch-policies</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/deployment-branch-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateDeploymentBranchPolicy(owner, repo, environmentName, body, callback)</td>
    <td style="padding:15px">repos/create-deployment-branch-policy</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/deployment-branch-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetDeploymentBranchPolicy(owner, repo, environmentName, branchPolicyId, callback)</td>
    <td style="padding:15px">repos/get-deployment-branch-policy</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/deployment-branch-policies/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposUpdateDeploymentBranchPolicy(owner, repo, environmentName, branchPolicyId, body, callback)</td>
    <td style="padding:15px">repos/update-deployment-branch-policy</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/deployment-branch-policies/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteDeploymentBranchPolicy(owner, repo, environmentName, branchPolicyId, callback)</td>
    <td style="padding:15px">repos/delete-deployment-branch-policy</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/deployment-branch-policies/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetAllDeploymentProtectionRules(environmentName, repo, owner, callback)</td>
    <td style="padding:15px">repos/get-all-deployment-protection-rules</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/deployment_protection_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateDeploymentProtectionRule(environmentName, repo, owner, body, callback)</td>
    <td style="padding:15px">repos/create-deployment-protection-rule</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/deployment_protection_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListCustomDeploymentRuleIntegrations(environmentName, repo, owner, page, perPage, callback)</td>
    <td style="padding:15px">repos/list-custom-deployment-rule-integrations</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/deployment_protection_rules/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetCustomDeploymentProtectionRule(owner, repo, environmentName, protectionRuleId, callback)</td>
    <td style="padding:15px">repos/get-custom-deployment-protection-rule</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/deployment_protection_rules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDisableDeploymentProtectionRule(environmentName, repo, owner, protectionRuleId, callback)</td>
    <td style="padding:15px">repos/disable-deployment-protection-rule</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/environments/{pathv3}/deployment_protection_rules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoForksQuery(owner, repo, sort, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-forks</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/forks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoHooksQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-webhooks</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetWebhookConfigForRepo(owner, repo, hookId, callback)</td>
    <td style="padding:15px">repos/get-webhook-config-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks/{pathv3}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposUpdateWebhookConfigForRepo(owner, repo, hookId, body, callback)</td>
    <td style="padding:15px">repos/update-webhook-config-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks/{pathv3}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListWebhookDeliveries(owner, repo, hookId, perPage, cursor, redelivery, callback)</td>
    <td style="padding:15px">repos/list-webhook-deliveries</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks/{pathv3}/deliveries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetWebhookDelivery(owner, repo, hookId, deliveryId, callback)</td>
    <td style="padding:15px">repos/get-webhook-delivery</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks/{pathv3}/deliveries/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposRedeliverWebhookDelivery(owner, repo, hookId, deliveryId, callback)</td>
    <td style="padding:15px">repos/redeliver-webhook-delivery</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks/{pathv3}/deliveries/{pathv4}/attempts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposPingWebhook(owner, repo, hookId, callback)</td>
    <td style="padding:15px">repos/ping-webhook</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/hooks/{pathv3}/pings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListInvitations(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-invitations</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/invitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposUpdateInvitation(owner, repo, invitationId, body, callback)</td>
    <td style="padding:15px">repos/update-invitation</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/invitations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteInvitation(owner, repo, invitationId, callback)</td>
    <td style="padding:15px">repos/delete-invitation</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/invitations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoKeysQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-deploy-keys</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposMergeUpstream(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/merge-upstream</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/merge-upstream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetPages(owner, repo, callback)</td>
    <td style="padding:15px">repos/get-pages</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreatePagesSite(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/create-pages-site</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposUpdateInformationAboutPagesSite(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/update-information-about-pages-site</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeletePagesSite(owner, repo, callback)</td>
    <td style="padding:15px">repos/delete-pages-site</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListPagesBuilds(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-pages-builds</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages/builds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposRequestPagesBuild(owner, repo, callback)</td>
    <td style="padding:15px">repos/request-pages-build</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages/builds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetLatestPagesBuild(owner, repo, callback)</td>
    <td style="padding:15px">repos/get-latest-pages-build</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages/builds/latest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetPagesBuild(owner, repo, buildId, callback)</td>
    <td style="padding:15px">repos/get-pages-build</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages/builds/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreatePagesDeployment(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/create-pages-deployment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetPagesDeployment(owner, repo, pagesDeploymentId, callback)</td>
    <td style="padding:15px">repos/get-pages-deployment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages/deployments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCancelPagesDeployment(owner, repo, pagesDeploymentId, callback)</td>
    <td style="padding:15px">repos/cancel-pages-deployment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages/deployments/{pathv3}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetPagesHealthCheck(owner, repo, callback)</td>
    <td style="padding:15px">repos/get-pages-health-check</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pages/health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCheckPrivateVulnerabilityReporting(owner, repo, callback)</td>
    <td style="padding:15px">repos/check-private-vulnerability-reporting</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/private-vulnerability-reporting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposEnablePrivateVulnerabilityReporting(owner, repo, callback)</td>
    <td style="padding:15px">repos/enable-private-vulnerability-reporting</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/private-vulnerability-reporting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDisablePrivateVulnerabilityReporting(owner, repo, callback)</td>
    <td style="padding:15px">repos/disable-private-vulnerability-reporting</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/private-vulnerability-reporting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetCustomPropertiesValues(owner, repo, callback)</td>
    <td style="padding:15px">repos/get-custom-properties-values</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/properties/values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateOrUpdateCustomPropertiesValues(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/create-or-update-custom-properties-values</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/properties/values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetReadmeInDirectory(owner, repo, dir, ref, callback)</td>
    <td style="padding:15px">repos/get-readme-in-directory</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/readme/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoReleasesQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-releases</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGenerateReleaseNotes(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/generate-release-notes</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/generate-notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetLatestRelease(owner, repo, callback)</td>
    <td style="padding:15px">repos/get-latest-release</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/latest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetReleaseByTag(owner, repo, tag, callback)</td>
    <td style="padding:15px">repos/get-release-by-tag</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListReleaseAssetsQuery(owner, repo, releaseId, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-release-assets</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/{pathv3}/assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposUploadReleaseAsset(owner, repo, releaseId, name, label, body, callback)</td>
    <td style="padding:15px">repos/upload-release-asset</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/{pathv3}/assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetBranchRules(owner, repo, branch, perPage, page, callback)</td>
    <td style="padding:15px">repos/get-branch-rules</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/rules/branches/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetRepoRulesets(owner, repo, perPage, page, includesParents, callback)</td>
    <td style="padding:15px">repos/get-repo-rulesets</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/rulesets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateRepoRuleset(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/create-repo-ruleset</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/rulesets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetRepoRuleSuites(owner, repo, ref, timePeriod, actorName, ruleSuiteResult, perPage, page, callback)</td>
    <td style="padding:15px">repos/get-repo-rule-suites</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/rulesets/rule-suites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetRepoRuleSuite(owner, repo, ruleSuiteId, callback)</td>
    <td style="padding:15px">repos/get-repo-rule-suite</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/rulesets/rule-suites/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetRepoRuleset(owner, repo, rulesetId, includesParents, callback)</td>
    <td style="padding:15px">repos/get-repo-ruleset</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/rulesets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposUpdateRepoRuleset(owner, repo, rulesetId, body, callback)</td>
    <td style="padding:15px">repos/update-repo-ruleset</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/rulesets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteRepoRuleset(owner, repo, rulesetId, callback)</td>
    <td style="padding:15px">repos/delete-repo-ruleset</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/rulesets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateCommitStatus(owner, repo, sha, body, callback)</td>
    <td style="padding:15px">repos/create-commit-status</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/statuses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoTagsQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-tags</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListTagProtection(owner, repo, callback)</td>
    <td style="padding:15px">repos/list-tag-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/tags/protection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateTagProtection(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/create-tag-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/tags/protection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeleteTagProtection(owner, repo, tagProtectionId, callback)</td>
    <td style="padding:15px">repos/delete-tag-protection</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/tags/protection/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDownloadTarballArchive(owner, repo, ref, callback)</td>
    <td style="padding:15px">repos/download-tarball-archive</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/tarball/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposOwnerRepoTeamsQuery(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-teams</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetAllTopics(owner, repo, page, perPage, callback)</td>
    <td style="padding:15px">repos/get-all-topics</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/topics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposReplaceAllTopics(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/replace-all-topics</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/topics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetClones(owner, repo, per, callback)</td>
    <td style="padding:15px">repos/get-clones</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/traffic/clones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetTopPaths(owner, repo, callback)</td>
    <td style="padding:15px">repos/get-top-paths</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/traffic/popular/paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetTopReferrers(owner, repo, callback)</td>
    <td style="padding:15px">repos/get-top-referrers</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/traffic/popular/referrers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposGetViews(owner, repo, per, callback)</td>
    <td style="padding:15px">repos/get-views</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/traffic/views?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposTransfer(owner, repo, body, callback)</td>
    <td style="padding:15px">repos/transfer</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/transfer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCheckVulnerabilityAlerts(owner, repo, callback)</td>
    <td style="padding:15px">repos/check-vulnerability-alerts</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/vulnerability-alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposEnableVulnerabilityAlerts(owner, repo, callback)</td>
    <td style="padding:15px">repos/enable-vulnerability-alerts</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/vulnerability-alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDisableVulnerabilityAlerts(owner, repo, callback)</td>
    <td style="padding:15px">repos/disable-vulnerability-alerts</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/vulnerability-alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDownloadZipballArchive(owner, repo, ref, callback)</td>
    <td style="padding:15px">repos/download-zipball-archive</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/zipball/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposCreateUsingTemplate(templateOwner, templateRepo, body, callback)</td>
    <td style="padding:15px">repos/create-using-template</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/generate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposListInvitationsForAuthenticatedUser(perPage, page, callback)</td>
    <td style="padding:15px">repos/list-invitations-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/repository_invitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposAcceptInvitationForAuthenticatedUser(invitationId, callback)</td>
    <td style="padding:15px">repos/accept-invitation-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/repository_invitations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reposDeclineInvitationForAuthenticatedUser(invitationId, callback)</td>
    <td style="padding:15px">repos/decline-invitation-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/repository_invitations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserStarredQuery(sort, direction, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-repos-starred-by-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/starred?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserSubscriptionsQuery(perPage, page, callback)</td>
    <td style="padding:15px">activity/list-watched-repos-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserIssuesQuery(filter, state, labels, sort, direction, since, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserOrgsQuery(perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserReposQuery(visibility, affiliation, type, sort, direction, perPage, page, since, before, callback)</td>
    <td style="padding:15px">repos/list-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/repos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserTeamsQuery(perPage, page, callback)</td>
    <td style="padding:15px">teams/list-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserEmailsQuery(perPage, page, callback)</td>
    <td style="padding:15px">users/list-emails-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/emails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserFollowersQuery(perPage, page, callback)</td>
    <td style="padding:15px">users/list-followers-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/followers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserFollowingQuery(perPage, page, callback)</td>
    <td style="padding:15px">users/list-followed-by-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/following?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserKeysQuery(perPage, page, callback)</td>
    <td style="padding:15px">users/list-public-ssh-keys-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameEventsQuery(username, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-events-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameEventsOrgsOrgQuery(username, org, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-org-events-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/events/orgs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameReceivedEventsQuery(username, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-received-events-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/received_events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameReceivedEventsPublicQuery(username, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-received-public-events-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/received_events/public?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameStarredQuery(username, sort, direction, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-repos-starred-by-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/starred?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameSubscriptionsQuery(username, perPage, page, callback)</td>
    <td style="padding:15px">activity/list-repos-watched-by-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameGistsQuery(username, since, perPage, page, callback)</td>
    <td style="padding:15px">gists/list-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/gists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameOrgsQuery(username, perPage, page, callback)</td>
    <td style="padding:15px">orgs/list-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameReposQuery(username, type, sort, direction, perPage, page, callback)</td>
    <td style="padding:15px">repos/list-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/repos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersListBlockedByAuthenticatedUser(perPage, page, callback)</td>
    <td style="padding:15px">users/list-blocked-by-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/blocks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersCheckBlocked(username, callback)</td>
    <td style="padding:15px">users/check-blocked</td>
    <td style="padding:15px">{base_path}/{version}/user/blocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersBlock(username, callback)</td>
    <td style="padding:15px">users/block</td>
    <td style="padding:15px">{base_path}/{version}/user/blocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUnblock(username, callback)</td>
    <td style="padding:15px">users/unblock</td>
    <td style="padding:15px">{base_path}/{version}/user/blocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersSetPrimaryEmailVisibilityForAuthenticatedUser(body, callback)</td>
    <td style="padding:15px">users/set-primary-email-visibility-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/email/visibility?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersListGpgKeysForAuthenticatedUser(perPage, page, callback)</td>
    <td style="padding:15px">users/list-gpg-keys-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/gpg_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersCreateGpgKeyForAuthenticatedUser(body, callback)</td>
    <td style="padding:15px">users/create-gpg-key-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/gpg_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersGetGpgKeyForAuthenticatedUser(gpgKeyId, callback)</td>
    <td style="padding:15px">users/get-gpg-key-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/gpg_keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersDeleteGpgKeyForAuthenticatedUser(gpgKeyId, callback)</td>
    <td style="padding:15px">users/delete-gpg-key-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/gpg_keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersListPublicEmailsForAuthenticatedUser(perPage, page, callback)</td>
    <td style="padding:15px">users/list-public-emails-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/public_emails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersListSocialAccountsForAuthenticatedUser(perPage, page, callback)</td>
    <td style="padding:15px">users/list-social-accounts-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/social_accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersAddSocialAccountForAuthenticatedUser(body, callback)</td>
    <td style="padding:15px">users/add-social-account-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/social_accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersDeleteSocialAccountForAuthenticatedUser(callback)</td>
    <td style="padding:15px">users/delete-social-account-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/social_accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersListSshSigningKeysForAuthenticatedUser(perPage, page, callback)</td>
    <td style="padding:15px">users/list-ssh-signing-keys-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/ssh_signing_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersCreateSshSigningKeyForAuthenticatedUser(body, callback)</td>
    <td style="padding:15px">users/create-ssh-signing-key-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/ssh_signing_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersGetSshSigningKeyForAuthenticatedUser(sshSigningKeyId, callback)</td>
    <td style="padding:15px">users/get-ssh-signing-key-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/ssh_signing_keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersDeleteSshSigningKeyForAuthenticatedUser(sshSigningKeyId, callback)</td>
    <td style="padding:15px">users/delete-ssh-signing-key-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/ssh_signing_keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersGetById(accountId, callback)</td>
    <td style="padding:15px">users/get-by-id</td>
    <td style="padding:15px">{base_path}/{version}/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersQuery(since, perPage, callback)</td>
    <td style="padding:15px">users/list</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersListAttestations(perPage, before, after, username, subjectDigest, callback)</td>
    <td style="padding:15px">users/list-attestations</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/attestations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameFollowersQuery(username, perPage, page, callback)</td>
    <td style="padding:15px">users/list-followers-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/followers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersListFollowingForUser(username, perPage, page, callback)</td>
    <td style="padding:15px">users/list-following-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/following?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersCheckFollowingForUser(username, targetUser, callback)</td>
    <td style="padding:15px">users/check-following-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/following/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersListGpgKeysForUser(username, perPage, page, callback)</td>
    <td style="padding:15px">users/list-gpg-keys-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/gpg_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersGetContextForUser(username, subjectType, subjectId, callback)</td>
    <td style="padding:15px">users/get-context-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/hovercard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameKeysQuery(username, perPage, page, callback)</td>
    <td style="padding:15px">users/list-public-keys-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersListSocialAccountsForUser(username, perPage, page, callback)</td>
    <td style="padding:15px">users/list-social-accounts-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/social_accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersListSshSigningKeysForUser(username, perPage, page, callback)</td>
    <td style="padding:15px">users/list-ssh-signing-keys-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/ssh_signing_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsGetAuthenticated(callback)</td>
    <td style="padding:15px">apps/get-authenticated</td>
    <td style="padding:15px">{base_path}/{version}/app?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsCreateFromManifest(code, callback)</td>
    <td style="padding:15px">apps/create-from-manifest</td>
    <td style="padding:15px">{base_path}/{version}/app-manifests/{pathv1}/conversions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsGetWebhookConfigForApp(callback)</td>
    <td style="padding:15px">apps/get-webhook-config-for-app</td>
    <td style="padding:15px">{base_path}/{version}/app/hook/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsUpdateWebhookConfigForApp(body, callback)</td>
    <td style="padding:15px">apps/update-webhook-config-for-app</td>
    <td style="padding:15px">{base_path}/{version}/app/hook/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListWebhookDeliveries(perPage, cursor, redelivery, callback)</td>
    <td style="padding:15px">apps/list-webhook-deliveries</td>
    <td style="padding:15px">{base_path}/{version}/app/hook/deliveries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsGetWebhookDelivery(deliveryId, callback)</td>
    <td style="padding:15px">apps/get-webhook-delivery</td>
    <td style="padding:15px">{base_path}/{version}/app/hook/deliveries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsRedeliverWebhookDelivery(deliveryId, callback)</td>
    <td style="padding:15px">apps/redeliver-webhook-delivery</td>
    <td style="padding:15px">{base_path}/{version}/app/hook/deliveries/{pathv1}/attempts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListInstallationRequestsForAuthenticatedApp(perPage, page, callback)</td>
    <td style="padding:15px">apps/list-installation-requests-for-authenticated-app</td>
    <td style="padding:15px">{base_path}/{version}/app/installation-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListInstallations(perPage, page, since, outdated, callback)</td>
    <td style="padding:15px">apps/list-installations</td>
    <td style="padding:15px">{base_path}/{version}/app/installations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsGetInstallation(installationId, callback)</td>
    <td style="padding:15px">apps/get-installation</td>
    <td style="padding:15px">{base_path}/{version}/app/installations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsDeleteInstallation(installationId, callback)</td>
    <td style="padding:15px">apps/delete-installation</td>
    <td style="padding:15px">{base_path}/{version}/app/installations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsCreateInstallationAccessToken(installationId, body, callback)</td>
    <td style="padding:15px">apps/create-installation-access-token</td>
    <td style="padding:15px">{base_path}/{version}/app/installations/{pathv1}/access_tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsSuspendInstallation(installationId, callback)</td>
    <td style="padding:15px">apps/suspend-installation</td>
    <td style="padding:15px">{base_path}/{version}/app/installations/{pathv1}/suspended?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsUnsuspendInstallation(installationId, callback)</td>
    <td style="padding:15px">apps/unsuspend-installation</td>
    <td style="padding:15px">{base_path}/{version}/app/installations/{pathv1}/suspended?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsDeleteAuthorization(clientId, callback)</td>
    <td style="padding:15px">apps/delete-authorization</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/grant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsCheckToken(clientId, body, callback)</td>
    <td style="padding:15px">apps/check-token</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsResetToken(clientId, body, callback)</td>
    <td style="padding:15px">apps/reset-token</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsDeleteToken(clientId, callback)</td>
    <td style="padding:15px">apps/delete-token</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsScopeToken(clientId, body, callback)</td>
    <td style="padding:15px">apps/scope-token</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/token/scoped?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsGetBySlug(appSlug, callback)</td>
    <td style="padding:15px">apps/get-by-slug</td>
    <td style="padding:15px">{base_path}/{version}/apps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListReposAccessibleToInstallation(perPage, page, callback)</td>
    <td style="padding:15px">apps/list-repos-accessible-to-installation</td>
    <td style="padding:15px">{base_path}/{version}/installation/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsRevokeInstallationAccessToken(callback)</td>
    <td style="padding:15px">apps/revoke-installation-access-token</td>
    <td style="padding:15px">{base_path}/{version}/installation/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsGetSubscriptionPlanForAccount(accountId, callback)</td>
    <td style="padding:15px">apps/get-subscription-plan-for-account</td>
    <td style="padding:15px">{base_path}/{version}/marketplace_listing/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListPlans(perPage, page, callback)</td>
    <td style="padding:15px">apps/list-plans</td>
    <td style="padding:15px">{base_path}/{version}/marketplace_listing/plans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListAccountsForPlan(planId, sort, direction, perPage, page, callback)</td>
    <td style="padding:15px">apps/list-accounts-for-plan</td>
    <td style="padding:15px">{base_path}/{version}/marketplace_listing/plans/{pathv1}/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsGetSubscriptionPlanForAccountStubbed(accountId, callback)</td>
    <td style="padding:15px">apps/get-subscription-plan-for-account-stubbed</td>
    <td style="padding:15px">{base_path}/{version}/marketplace_listing/stubbed/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListPlansStubbed(perPage, page, callback)</td>
    <td style="padding:15px">apps/list-plans-stubbed</td>
    <td style="padding:15px">{base_path}/{version}/marketplace_listing/stubbed/plans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListAccountsForPlanStubbed(planId, sort, direction, perPage, page, callback)</td>
    <td style="padding:15px">apps/list-accounts-for-plan-stubbed</td>
    <td style="padding:15px">{base_path}/{version}/marketplace_listing/stubbed/plans/{pathv1}/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsGetOrgInstallation(org, callback)</td>
    <td style="padding:15px">apps/get-org-installation</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/installation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsGetRepoInstallation(owner, repo, callback)</td>
    <td style="padding:15px">apps/get-repo-installation</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/installation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListInstallationsForAuthenticatedUser(perPage, page, callback)</td>
    <td style="padding:15px">apps/list-installations-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/installations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListInstallationReposForAuthenticatedUser(installationId, perPage, page, callback)</td>
    <td style="padding:15px">apps/list-installation-repos-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/installations/{pathv1}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsAddRepoToInstallationForAuthenticatedUser(installationId, repositoryId, callback)</td>
    <td style="padding:15px">apps/add-repo-to-installation-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/installations/{pathv1}/repositories/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsRemoveRepoFromInstallationForAuthenticatedUser(installationId, repositoryId, callback)</td>
    <td style="padding:15px">apps/remove-repo-from-installation-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/installations/{pathv1}/repositories/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListSubscriptionsForAuthenticatedUser(perPage, page, callback)</td>
    <td style="padding:15px">apps/list-subscriptions-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/marketplace_purchases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsListSubscriptionsForAuthenticatedUserStubbed(perPage, page, callback)</td>
    <td style="padding:15px">apps/list-subscriptions-for-authenticated-user-stubbed</td>
    <td style="padding:15px">{base_path}/{version}/user/marketplace_purchases/stubbed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsGetUserInstallation(username, callback)</td>
    <td style="padding:15px">apps/get-user-installation</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/installation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">billingGetGithubActionsBillingOrg(org, callback)</td>
    <td style="padding:15px">billing/get-github-actions-billing-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/settings/billing/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">billingGetGithubPackagesBillingOrg(org, callback)</td>
    <td style="padding:15px">billing/get-github-packages-billing-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/settings/billing/packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">billingGetSharedStorageBillingOrg(org, callback)</td>
    <td style="padding:15px">billing/get-shared-storage-billing-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/settings/billing/shared-storage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">billingGetGithubActionsBillingUser(username, callback)</td>
    <td style="padding:15px">billing/get-github-actions-billing-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/settings/billing/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">billingGetGithubPackagesBillingUser(username, callback)</td>
    <td style="padding:15px">billing/get-github-packages-billing-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/settings/billing/packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">billingGetSharedStorageBillingUser(username, callback)</td>
    <td style="padding:15px">billing/get-shared-storage-billing-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/settings/billing/shared-storage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksCreate(owner, repo, body, callback)</td>
    <td style="padding:15px">checks/create</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/check-runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksGet(owner, repo, checkRunId, callback)</td>
    <td style="padding:15px">checks/get</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/check-runs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksUpdate(owner, repo, checkRunId, body, callback)</td>
    <td style="padding:15px">checks/update</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/check-runs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksListAnnotations(owner, repo, checkRunId, perPage, page, callback)</td>
    <td style="padding:15px">checks/list-annotations</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/check-runs/{pathv3}/annotations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksRerequestRun(owner, repo, checkRunId, callback)</td>
    <td style="padding:15px">checks/rerequest-run</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/check-runs/{pathv3}/rerequest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksCreateSuite(owner, repo, body, callback)</td>
    <td style="padding:15px">checks/create-suite</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/check-suites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksSetSuitesPreferences(owner, repo, body, callback)</td>
    <td style="padding:15px">checks/set-suites-preferences</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/check-suites/preferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksGetSuite(owner, repo, checkSuiteId, callback)</td>
    <td style="padding:15px">checks/get-suite</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/check-suites/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksListForSuite(owner, repo, checkSuiteId, checkName, status, filter, perPage, page, callback)</td>
    <td style="padding:15px">checks/list-for-suite</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/check-suites/{pathv3}/check-runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksRerequestSuite(owner, repo, checkSuiteId, callback)</td>
    <td style="padding:15px">checks/rerequest-suite</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/check-suites/{pathv3}/rerequest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksListForRef(owner, repo, ref, checkName, status, filter, perPage, page, appId, callback)</td>
    <td style="padding:15px">checks/list-for-ref</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/commits/{pathv3}/check-runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checksListSuitesForRef(owner, repo, ref, appId, checkName, perPage, page, callback)</td>
    <td style="padding:15px">checks/list-suites-for-ref</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/commits/{pathv3}/check-suites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningListAlertsForOrg(org, toolName, toolGuid, before, after, page, perPage, direction, state, sort, severity, callback)</td>
    <td style="padding:15px">code-scanning/list-alerts-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/code-scanning/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningListAlertsForRepo(owner, repo, toolName, toolGuid, page, perPage, ref, direction, sort, state, severity, callback)</td>
    <td style="padding:15px">code-scanning/list-alerts-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningGetAlert(owner, repo, alertNumber, callback)</td>
    <td style="padding:15px">code-scanning/get-alert</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/alerts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningUpdateAlert(owner, repo, alertNumber, body, callback)</td>
    <td style="padding:15px">code-scanning/update-alert</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/alerts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningListAlertInstances(owner, repo, alertNumber, page, perPage, ref, callback)</td>
    <td style="padding:15px">code-scanning/list-alert-instances</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/alerts/{pathv3}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningListRecentAnalyses(owner, repo, toolName, toolGuid, page, perPage, ref, sarifId, direction, sort, callback)</td>
    <td style="padding:15px">code-scanning/list-recent-analyses</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/analyses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningGetAnalysis(owner, repo, analysisId, callback)</td>
    <td style="padding:15px">code-scanning/get-analysis</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/analyses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningDeleteAnalysis(owner, repo, analysisId, confirmDelete, callback)</td>
    <td style="padding:15px">code-scanning/delete-analysis</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/analyses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningListCodeqlDatabases(owner, repo, callback)</td>
    <td style="padding:15px">code-scanning/list-codeql-databases</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/codeql/databases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningGetCodeqlDatabase(owner, repo, language, callback)</td>
    <td style="padding:15px">code-scanning/get-codeql-database</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/codeql/databases/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningCreateVariantAnalysis(owner, repo, body, callback)</td>
    <td style="padding:15px">code-scanning/create-variant-analysis</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/codeql/variant-analyses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningGetVariantAnalysis(owner, repo, codeqlVariantAnalysisId, callback)</td>
    <td style="padding:15px">code-scanning/get-variant-analysis</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/codeql/variant-analyses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningGetVariantAnalysisRepoTask(owner, repo, codeqlVariantAnalysisId, repoOwner, repoName, callback)</td>
    <td style="padding:15px">code-scanning/get-variant-analysis-repo-task</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/codeql/variant-analyses/{pathv3}/repos/{pathv4}/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningGetDefaultSetup(owner, repo, callback)</td>
    <td style="padding:15px">code-scanning/get-default-setup</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/default-setup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningUpdateDefaultSetup(owner, repo, body, callback)</td>
    <td style="padding:15px">code-scanning/update-default-setup</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/default-setup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningUploadSarif(owner, repo, body, callback)</td>
    <td style="padding:15px">code-scanning/upload-sarif</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/sarifs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeScanningGetSarif(owner, repo, sarifId, callback)</td>
    <td style="padding:15px">code-scanning/get-sarif</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-scanning/sarifs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codesOfConductGetAllCodesOfConduct(callback)</td>
    <td style="padding:15px">codes-of-conduct/get-all-codes-of-conduct</td>
    <td style="padding:15px">{base_path}/{version}/codes_of_conduct?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codesOfConductGetConductCode(key, callback)</td>
    <td style="padding:15px">codes-of-conduct/get-conduct-code</td>
    <td style="padding:15px">{base_path}/{version}/codes_of_conduct/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotListAlertsForEnterprise(enterprise, state, severity, ecosystem, packageParam, scope, sort, direction, before, after, first, last, perPage, callback)</td>
    <td style="padding:15px">dependabot/list-alerts-for-enterprise</td>
    <td style="padding:15px">{base_path}/{version}/enterprises/{pathv1}/dependabot/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotListAlertsForOrg(org, state, severity, ecosystem, packageParam, scope, sort, direction, before, after, first, last, perPage, callback)</td>
    <td style="padding:15px">dependabot/list-alerts-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/dependabot/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotListOrgSecrets(org, perPage, page, callback)</td>
    <td style="padding:15px">dependabot/list-org-secrets</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/dependabot/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotGetOrgPublicKey(org, callback)</td>
    <td style="padding:15px">dependabot/get-org-public-key</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/dependabot/secrets/public-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotGetOrgSecret(org, secretName, callback)</td>
    <td style="padding:15px">dependabot/get-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/dependabot/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotCreateOrUpdateOrgSecret(org, secretName, body, callback)</td>
    <td style="padding:15px">dependabot/create-or-update-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/dependabot/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotDeleteOrgSecret(org, secretName, callback)</td>
    <td style="padding:15px">dependabot/delete-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/dependabot/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotListSelectedReposForOrgSecret(org, secretName, page, perPage, callback)</td>
    <td style="padding:15px">dependabot/list-selected-repos-for-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/dependabot/secrets/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotSetSelectedReposForOrgSecret(org, secretName, body, callback)</td>
    <td style="padding:15px">dependabot/set-selected-repos-for-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/dependabot/secrets/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotAddSelectedRepoToOrgSecret(org, secretName, repositoryId, callback)</td>
    <td style="padding:15px">dependabot/add-selected-repo-to-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/dependabot/secrets/{pathv2}/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotRemoveSelectedRepoFromOrgSecret(org, secretName, repositoryId, callback)</td>
    <td style="padding:15px">dependabot/remove-selected-repo-from-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/dependabot/secrets/{pathv2}/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotListAlertsForRepo(owner, repo, state, severity, ecosystem, packageParam, manifest, scope, sort, direction, page, perPage, before, after, first, last, callback)</td>
    <td style="padding:15px">dependabot/list-alerts-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dependabot/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotGetAlert(owner, repo, alertNumber, callback)</td>
    <td style="padding:15px">dependabot/get-alert</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dependabot/alerts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotUpdateAlert(owner, repo, alertNumber, body, callback)</td>
    <td style="padding:15px">dependabot/update-alert</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dependabot/alerts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotListRepoSecrets(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">dependabot/list-repo-secrets</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dependabot/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotGetRepoPublicKey(owner, repo, callback)</td>
    <td style="padding:15px">dependabot/get-repo-public-key</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dependabot/secrets/public-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotGetRepoSecret(owner, repo, secretName, callback)</td>
    <td style="padding:15px">dependabot/get-repo-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dependabot/secrets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotCreateOrUpdateRepoSecret(owner, repo, secretName, body, callback)</td>
    <td style="padding:15px">dependabot/create-or-update-repo-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dependabot/secrets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependabotDeleteRepoSecret(owner, repo, secretName, callback)</td>
    <td style="padding:15px">dependabot/delete-repo-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dependabot/secrets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependencyGraphDiffRange(owner, repo, basehead, name, callback)</td>
    <td style="padding:15px">dependency-graph/diff-range</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dependency-graph/compare/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependencyGraphExportSbom(owner, repo, callback)</td>
    <td style="padding:15px">dependency-graph/export-sbom</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dependency-graph/sbom?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dependencyGraphCreateRepositorySnapshot(owner, repo, body, callback)</td>
    <td style="padding:15px">dependency-graph/create-repository-snapshot</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/dependency-graph/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGistsQuery(since, perPage, page, callback)</td>
    <td style="padding:15px">gists/list</td>
    <td style="padding:15px">{base_path}/{version}/gists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGistsPublicQuery(since, perPage, page, callback)</td>
    <td style="padding:15px">gists/list-public</td>
    <td style="padding:15px">{base_path}/{version}/gists/public?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGistsStarredQuery(since, perPage, page, callback)</td>
    <td style="padding:15px">gists/list-starred</td>
    <td style="padding:15px">{base_path}/{version}/gists/starred?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGistsIdCommentsQuery(gistId, perPage, page, callback)</td>
    <td style="padding:15px">gists/list-comments</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gistsListCommits(gistId, perPage, page, callback)</td>
    <td style="padding:15px">gists/list-commits</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gistsListForks(gistId, perPage, page, callback)</td>
    <td style="padding:15px">gists/list-forks</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/forks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gistsGetRevision(gistId, sha, callback)</td>
    <td style="padding:15px">gists/get-revision</td>
    <td style="padding:15px">{base_path}/{version}/gists/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gitGetRef(owner, repo, ref, callback)</td>
    <td style="padding:15px">git/get-ref</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/git/ref/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gitignoreGetTemplate(name, callback)</td>
    <td style="padding:15px">gitignore/get-template</td>
    <td style="padding:15px">{base_path}/{version}/gitignore/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssuesQuery(filter, state, labels, sort, direction, since, collab, orgs, owned, pulls, perPage, page, callback)</td>
    <td style="padding:15px">issues/list</td>
    <td style="padding:15px">{base_path}/{version}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">issuesAddAssignees(owner, repo, issueNumber, body, callback)</td>
    <td style="padding:15px">issues/add-assignees</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/assignees?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">issuesRemoveAssignees(owner, repo, issueNumber, callback)</td>
    <td style="padding:15px">issues/remove-assignees</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/assignees?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">issuesCheckUserCanBeAssignedToIssue(owner, repo, issueNumber, assignee, callback)</td>
    <td style="padding:15px">issues/check-user-can-be-assigned-to-issue</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/assignees/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">issuesLock(owner, repo, issueNumber, body, callback)</td>
    <td style="padding:15px">issues/lock</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/lock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">issuesUnlock(owner, repo, issueNumber, callback)</td>
    <td style="padding:15px">issues/unlock</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/lock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">issuesListEventsForTimeline(owner, repo, issueNumber, perPage, page, callback)</td>
    <td style="padding:15px">issues/list-events-for-timeline</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/timeline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">licensesGetAllCommonlyUsed(featured, perPage, page, callback)</td>
    <td style="padding:15px">licenses/get-all-commonly-used</td>
    <td style="padding:15px">{base_path}/{version}/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">licensesGet(license, callback)</td>
    <td style="padding:15px">licenses/get</td>
    <td style="padding:15px">{base_path}/{version}/licenses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">licensesGetForRepo(owner, repo, ref, callback)</td>
    <td style="padding:15px">licenses/get-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metaRoot(callback)</td>
    <td style="padding:15px">meta/root</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metaGetOctocat(s, callback)</td>
    <td style="padding:15px">meta/get-octocat</td>
    <td style="padding:15px">{base_path}/{version}/octocat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metaGetAllVersions(callback)</td>
    <td style="padding:15px">meta/get-all-versions</td>
    <td style="padding:15px">{base_path}/{version}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metaGetZen(callback)</td>
    <td style="padding:15px">meta/get-zen</td>
    <td style="padding:15px">{base_path}/{version}/zen?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsListForOrg(org, perPage, page, exclude, callback)</td>
    <td style="padding:15px">migrations/list-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/migrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsStartForOrg(org, body, callback)</td>
    <td style="padding:15px">migrations/start-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/migrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsGetStatusForOrg(org, migrationId, exclude, callback)</td>
    <td style="padding:15px">migrations/get-status-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/migrations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsDownloadArchiveForOrg(org, migrationId, callback)</td>
    <td style="padding:15px">migrations/download-archive-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/migrations/{pathv2}/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsDeleteArchiveForOrg(org, migrationId, callback)</td>
    <td style="padding:15px">migrations/delete-archive-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/migrations/{pathv2}/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsUnlockRepoForOrg(org, migrationId, repoName, callback)</td>
    <td style="padding:15px">migrations/unlock-repo-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/migrations/{pathv2}/repos/{pathv3}/lock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsListReposForOrg(org, migrationId, perPage, page, callback)</td>
    <td style="padding:15px">migrations/list-repos-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/migrations/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsGetImportStatus(owner, repo, callback)</td>
    <td style="padding:15px">migrations/get-import-status</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsStartImport(owner, repo, body, callback)</td>
    <td style="padding:15px">migrations/start-import</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsUpdateImport(owner, repo, body, callback)</td>
    <td style="padding:15px">migrations/update-import</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsCancelImport(owner, repo, callback)</td>
    <td style="padding:15px">migrations/cancel-import</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsGetCommitAuthors(owner, repo, since, callback)</td>
    <td style="padding:15px">migrations/get-commit-authors</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/import/authors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsMapCommitAuthor(owner, repo, authorId, body, callback)</td>
    <td style="padding:15px">migrations/map-commit-author</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/import/authors/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsGetLargeFiles(owner, repo, callback)</td>
    <td style="padding:15px">migrations/get-large-files</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/import/large_files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsSetLfsPreference(owner, repo, body, callback)</td>
    <td style="padding:15px">migrations/set-lfs-preference</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/import/lfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsListForAuthenticatedUser(perPage, page, callback)</td>
    <td style="padding:15px">migrations/list-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/migrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsStartForAuthenticatedUser(body, callback)</td>
    <td style="padding:15px">migrations/start-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/migrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsGetStatusForAuthenticatedUser(migrationId, exclude, callback)</td>
    <td style="padding:15px">migrations/get-status-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/migrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsGetArchiveForAuthenticatedUser(migrationId, callback)</td>
    <td style="padding:15px">migrations/get-archive-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/migrations/{pathv1}/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsDeleteArchiveForAuthenticatedUser(migrationId, callback)</td>
    <td style="padding:15px">migrations/delete-archive-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/migrations/{pathv1}/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsUnlockRepoForAuthenticatedUser(migrationId, repoName, callback)</td>
    <td style="padding:15px">migrations/unlock-repo-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/migrations/{pathv1}/repos/{pathv2}/lock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationsListReposForAuthenticatedUser(migrationId, perPage, page, callback)</td>
    <td style="padding:15px">migrations/list-repos-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/migrations/{pathv1}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oidcGetOidcCustomSubTemplateForOrg(org, callback)</td>
    <td style="padding:15px">oidc/get-oidc-custom-sub-template-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/oidc/customization/sub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oidcUpdateOidcCustomSubTemplateForOrg(org, body, callback)</td>
    <td style="padding:15px">oidc/update-oidc-custom-sub-template-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/actions/oidc/customization/sub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesListDockerMigrationConflictingPackagesForOrganization(org, callback)</td>
    <td style="padding:15px">packages/list-docker-migration-conflicting-packages-for-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/docker/conflicts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesListPackagesForOrganization(packageType, org, visibility, page, perPage, callback)</td>
    <td style="padding:15px">packages/list-packages-for-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesGetPackageForOrganization(packageType, packageName, org, callback)</td>
    <td style="padding:15px">packages/get-package-for-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/packages/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesDeletePackageForOrg(packageType, packageName, org, callback)</td>
    <td style="padding:15px">packages/delete-package-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/packages/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesRestorePackageForOrg(packageType, packageName, org, token, callback)</td>
    <td style="padding:15px">packages/restore-package-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/packages/{pathv2}/{pathv3}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesGetAllPackageVersionsForPackageOwnedByOrg(packageType, packageName, org, page, perPage, state, callback)</td>
    <td style="padding:15px">packages/get-all-package-versions-for-package-owned-by-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/packages/{pathv2}/{pathv3}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesGetPackageVersionForOrganization(packageType, packageName, org, packageVersionId, callback)</td>
    <td style="padding:15px">packages/get-package-version-for-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/packages/{pathv2}/{pathv3}/versions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesDeletePackageVersionForOrg(packageType, packageName, org, packageVersionId, callback)</td>
    <td style="padding:15px">packages/delete-package-version-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/packages/{pathv2}/{pathv3}/versions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesRestorePackageVersionForOrg(packageType, packageName, org, packageVersionId, callback)</td>
    <td style="padding:15px">packages/restore-package-version-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/packages/{pathv2}/{pathv3}/versions/{pathv4}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesListDockerMigrationConflictingPackagesForAuthenticatedUser(callback)</td>
    <td style="padding:15px">packages/list-docker-migration-conflicting-packages-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/docker/conflicts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesListPackagesForAuthenticatedUser(packageType, visibility, page, perPage, callback)</td>
    <td style="padding:15px">packages/list-packages-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesGetPackageForAuthenticatedUser(packageType, packageName, callback)</td>
    <td style="padding:15px">packages/get-package-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/packages/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesDeletePackageForAuthenticatedUser(packageType, packageName, callback)</td>
    <td style="padding:15px">packages/delete-package-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/packages/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesRestorePackageForAuthenticatedUser(packageType, packageName, token, callback)</td>
    <td style="padding:15px">packages/restore-package-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/packages/{pathv1}/{pathv2}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesGetAllPackageVersionsForPackageOwnedByAuthenticatedUser(packageType, packageName, page, perPage, state, callback)</td>
    <td style="padding:15px">packages/get-all-package-versions-for-package-owned-by-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/packages/{pathv1}/{pathv2}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesGetPackageVersionForAuthenticatedUser(packageType, packageName, packageVersionId, callback)</td>
    <td style="padding:15px">packages/get-package-version-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/packages/{pathv1}/{pathv2}/versions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesDeletePackageVersionForAuthenticatedUser(packageType, packageName, packageVersionId, callback)</td>
    <td style="padding:15px">packages/delete-package-version-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/packages/{pathv1}/{pathv2}/versions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesRestorePackageVersionForAuthenticatedUser(packageType, packageName, packageVersionId, callback)</td>
    <td style="padding:15px">packages/restore-package-version-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/packages/{pathv1}/{pathv2}/versions/{pathv3}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesListDockerMigrationConflictingPackagesForUser(username, callback)</td>
    <td style="padding:15px">packages/list-docker-migration-conflicting-packages-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/docker/conflicts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesListPackagesForUser(packageType, visibility, username, page, perPage, callback)</td>
    <td style="padding:15px">packages/list-packages-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesGetPackageForUser(packageType, packageName, username, callback)</td>
    <td style="padding:15px">packages/get-package-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/packages/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesDeletePackageForUser(packageType, packageName, username, callback)</td>
    <td style="padding:15px">packages/delete-package-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/packages/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesRestorePackageForUser(packageType, packageName, username, token, callback)</td>
    <td style="padding:15px">packages/restore-package-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/packages/{pathv2}/{pathv3}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesGetAllPackageVersionsForPackageOwnedByUser(packageType, packageName, username, callback)</td>
    <td style="padding:15px">packages/get-all-package-versions-for-package-owned-by-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/packages/{pathv2}/{pathv3}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesGetPackageVersionForUser(packageType, packageName, packageVersionId, username, callback)</td>
    <td style="padding:15px">packages/get-package-version-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/packages/{pathv2}/{pathv3}/versions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesDeletePackageVersionForUser(packageType, packageName, username, packageVersionId, callback)</td>
    <td style="padding:15px">packages/delete-package-version-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/packages/{pathv2}/{pathv3}/versions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packagesRestorePackageVersionForUser(packageType, packageName, username, packageVersionId, callback)</td>
    <td style="padding:15px">packages/restore-package-version-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/packages/{pathv2}/{pathv3}/versions/{pathv4}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsListForOrg(org, state, perPage, page, callback)</td>
    <td style="padding:15px">projects/list-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsCreateForOrg(org, body, callback)</td>
    <td style="padding:15px">projects/create-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsGetCard(cardId, callback)</td>
    <td style="padding:15px">projects/get-card</td>
    <td style="padding:15px">{base_path}/{version}/projects/columns/cards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsUpdateCard(cardId, body, callback)</td>
    <td style="padding:15px">projects/update-card</td>
    <td style="padding:15px">{base_path}/{version}/projects/columns/cards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsDeleteCard(cardId, callback)</td>
    <td style="padding:15px">projects/delete-card</td>
    <td style="padding:15px">{base_path}/{version}/projects/columns/cards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsMoveCard(cardId, body, callback)</td>
    <td style="padding:15px">projects/move-card</td>
    <td style="padding:15px">{base_path}/{version}/projects/columns/cards/{pathv1}/moves?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsGetColumn(columnId, callback)</td>
    <td style="padding:15px">projects/get-column</td>
    <td style="padding:15px">{base_path}/{version}/projects/columns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsUpdateColumn(columnId, body, callback)</td>
    <td style="padding:15px">projects/update-column</td>
    <td style="padding:15px">{base_path}/{version}/projects/columns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsDeleteColumn(columnId, callback)</td>
    <td style="padding:15px">projects/delete-column</td>
    <td style="padding:15px">{base_path}/{version}/projects/columns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsListCards(columnId, archivedState, perPage, page, callback)</td>
    <td style="padding:15px">projects/list-cards</td>
    <td style="padding:15px">{base_path}/{version}/projects/columns/{pathv1}/cards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsCreateCard(columnId, body, callback)</td>
    <td style="padding:15px">projects/create-card</td>
    <td style="padding:15px">{base_path}/{version}/projects/columns/{pathv1}/cards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsMoveColumn(columnId, body, callback)</td>
    <td style="padding:15px">projects/move-column</td>
    <td style="padding:15px">{base_path}/{version}/projects/columns/{pathv1}/moves?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsGet(projectId, callback)</td>
    <td style="padding:15px">projects/get</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsUpdate(projectId, body, callback)</td>
    <td style="padding:15px">projects/update</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsDelete(projectId, callback)</td>
    <td style="padding:15px">projects/delete</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsListCollaborators(projectId, affiliation, perPage, page, callback)</td>
    <td style="padding:15px">projects/list-collaborators</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/collaborators?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsAddCollaborator(projectId, username, body, callback)</td>
    <td style="padding:15px">projects/add-collaborator</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/collaborators/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsRemoveCollaborator(projectId, username, callback)</td>
    <td style="padding:15px">projects/remove-collaborator</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/collaborators/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsGetPermissionForUser(projectId, username, callback)</td>
    <td style="padding:15px">projects/get-permission-for-user</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/collaborators/{pathv2}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsListColumns(projectId, perPage, page, callback)</td>
    <td style="padding:15px">projects/list-columns</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsCreateColumn(projectId, body, callback)</td>
    <td style="padding:15px">projects/create-column</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsListForRepo(owner, repo, state, perPage, page, callback)</td>
    <td style="padding:15px">projects/list-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsCreateForRepo(owner, repo, body, callback)</td>
    <td style="padding:15px">projects/create-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsCreateForAuthenticatedUser(body, callback)</td>
    <td style="padding:15px">projects/create-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsListForUser(username, state, perPage, page, callback)</td>
    <td style="padding:15px">projects/list-for-user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsCreateReplyForReviewComment(owner, repo, pullNumber, commentId, body, callback)</td>
    <td style="padding:15px">pulls/create-reply-for-review-comment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/comments/{pathv4}/replies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsListRequestedReviewers(owner, repo, pullNumber, callback)</td>
    <td style="padding:15px">pulls/list-requested-reviewers</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/requested_reviewers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsRequestReviewers(owner, repo, pullNumber, body, callback)</td>
    <td style="padding:15px">pulls/request-reviewers</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/requested_reviewers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsRemoveRequestedReviewers(owner, repo, pullNumber, callback)</td>
    <td style="padding:15px">pulls/remove-requested-reviewers</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/requested_reviewers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsListReviews(owner, repo, pullNumber, perPage, page, callback)</td>
    <td style="padding:15px">pulls/list-reviews</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/reviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsCreateReview(owner, repo, pullNumber, body, callback)</td>
    <td style="padding:15px">pulls/create-review</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/reviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsGetReview(owner, repo, pullNumber, reviewId, callback)</td>
    <td style="padding:15px">pulls/get-review</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/reviews/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsUpdateReview(owner, repo, pullNumber, reviewId, body, callback)</td>
    <td style="padding:15px">pulls/update-review</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/reviews/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsDeletePendingReview(owner, repo, pullNumber, reviewId, callback)</td>
    <td style="padding:15px">pulls/delete-pending-review</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/reviews/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsListCommentsForReview(owner, repo, pullNumber, reviewId, perPage, page, callback)</td>
    <td style="padding:15px">pulls/list-comments-for-review</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/reviews/{pathv4}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsDismissReview(owner, repo, pullNumber, reviewId, body, callback)</td>
    <td style="padding:15px">pulls/dismiss-review</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/reviews/{pathv4}/dismissals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsSubmitReview(owner, repo, pullNumber, reviewId, body, callback)</td>
    <td style="padding:15px">pulls/submit-review</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/reviews/{pathv4}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullsUpdateBranch(owner, repo, pullNumber, body, callback)</td>
    <td style="padding:15px">pulls/update-branch</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/update-branch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsListForTeamDiscussionCommentInOrg(org, teamSlug, discussionNumber, commentNumber, content, perPage, page, callback)</td>
    <td style="padding:15px">reactions/list-for-team-discussion-comment-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}/comments/{pathv4}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsCreateForTeamDiscussionCommentInOrg(org, teamSlug, discussionNumber, commentNumber, body, callback)</td>
    <td style="padding:15px">reactions/create-for-team-discussion-comment-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}/comments/{pathv4}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsDeleteForTeamDiscussionComment(org, teamSlug, discussionNumber, commentNumber, reactionId, callback)</td>
    <td style="padding:15px">reactions/delete-for-team-discussion-comment</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}/comments/{pathv4}/reactions/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsListForTeamDiscussionInOrg(org, teamSlug, discussionNumber, content, perPage, page, callback)</td>
    <td style="padding:15px">reactions/list-for-team-discussion-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsCreateForTeamDiscussionInOrg(org, teamSlug, discussionNumber, body, callback)</td>
    <td style="padding:15px">reactions/create-for-team-discussion-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsDeleteForTeamDiscussion(org, teamSlug, discussionNumber, reactionId, callback)</td>
    <td style="padding:15px">reactions/delete-for-team-discussion</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}/reactions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsListForCommitComment(owner, repo, commentId, content, perPage, page, callback)</td>
    <td style="padding:15px">reactions/list-for-commit-comment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/comments/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsCreateForCommitComment(owner, repo, commentId, body, callback)</td>
    <td style="padding:15px">reactions/create-for-commit-comment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/comments/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsDeleteForCommitComment(owner, repo, commentId, reactionId, callback)</td>
    <td style="padding:15px">reactions/delete-for-commit-comment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/comments/{pathv3}/reactions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsListForIssueComment(owner, repo, commentId, content, perPage, page, callback)</td>
    <td style="padding:15px">reactions/list-for-issue-comment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/comments/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsCreateForIssueComment(owner, repo, commentId, body, callback)</td>
    <td style="padding:15px">reactions/create-for-issue-comment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/comments/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsDeleteForIssueComment(owner, repo, commentId, reactionId, callback)</td>
    <td style="padding:15px">reactions/delete-for-issue-comment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/comments/{pathv3}/reactions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsListForIssue(owner, repo, issueNumber, content, perPage, page, callback)</td>
    <td style="padding:15px">reactions/list-for-issue</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsCreateForIssue(owner, repo, issueNumber, body, callback)</td>
    <td style="padding:15px">reactions/create-for-issue</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsDeleteForIssue(owner, repo, issueNumber, reactionId, callback)</td>
    <td style="padding:15px">reactions/delete-for-issue</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/issues/{pathv3}/reactions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsListForPullRequestReviewComment(owner, repo, commentId, content, perPage, page, callback)</td>
    <td style="padding:15px">reactions/list-for-pull-request-review-comment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/comments/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsCreateForPullRequestReviewComment(owner, repo, commentId, body, callback)</td>
    <td style="padding:15px">reactions/create-for-pull-request-review-comment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/comments/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsDeleteForPullRequestComment(owner, repo, commentId, reactionId, callback)</td>
    <td style="padding:15px">reactions/delete-for-pull-request-comment</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/comments/{pathv3}/reactions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsListForRelease(owner, repo, releaseId, content, perPage, page, callback)</td>
    <td style="padding:15px">reactions/list-for-release</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsCreateForRelease(owner, repo, releaseId, body, callback)</td>
    <td style="padding:15px">reactions/create-for-release</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsDeleteForRelease(owner, repo, releaseId, reactionId, callback)</td>
    <td style="padding:15px">reactions/delete-for-release</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/releases/{pathv3}/reactions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsListForTeamDiscussionCommentLegacy(teamId, discussionNumber, commentNumber, content, perPage, page, callback)</td>
    <td style="padding:15px">reactions/list-for-team-discussion-comment-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}/comments/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsCreateForTeamDiscussionCommentLegacy(teamId, discussionNumber, commentNumber, body, callback)</td>
    <td style="padding:15px">reactions/create-for-team-discussion-comment-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}/comments/{pathv3}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsListForTeamDiscussionLegacy(teamId, discussionNumber, content, perPage, page, callback)</td>
    <td style="padding:15px">reactions/list-for-team-discussion-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactionsCreateForTeamDiscussionLegacy(teamId, discussionNumber, body, callback)</td>
    <td style="padding:15px">reactions/create-for-team-discussion-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}/reactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchCommits(q, sort, order, perPage, page, callback)</td>
    <td style="padding:15px">search/commits</td>
    <td style="padding:15px">{base_path}/{version}/search/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchLabels(repositoryId, q, sort, order, perPage, page, callback)</td>
    <td style="padding:15px">search/labels</td>
    <td style="padding:15px">{base_path}/{version}/search/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchRepositoriesQuery(q, sort, order, perPage, page, callback)</td>
    <td style="padding:15px">search/repos</td>
    <td style="padding:15px">{base_path}/{version}/search/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchTopics(q, perPage, page, callback)</td>
    <td style="padding:15px">search/topics</td>
    <td style="padding:15px">{base_path}/{version}/search/topics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchUsersQuery(q, sort, order, perPage, page, callback)</td>
    <td style="padding:15px">search/users</td>
    <td style="padding:15px">{base_path}/{version}/search/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretScanningListAlertsForEnterprise(enterprise, state, secretType, resolution, sort, direction, perPage, before, after, validity, callback)</td>
    <td style="padding:15px">secret-scanning/list-alerts-for-enterprise</td>
    <td style="padding:15px">{base_path}/{version}/enterprises/{pathv1}/secret-scanning/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretScanningListAlertsForOrg(org, state, secretType, resolution, sort, direction, page, perPage, before, after, validity, callback)</td>
    <td style="padding:15px">secret-scanning/list-alerts-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/secret-scanning/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretScanningListAlertsForRepo(owner, repo, state, secretType, resolution, sort, direction, page, perPage, before, after, validity, callback)</td>
    <td style="padding:15px">secret-scanning/list-alerts-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/secret-scanning/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretScanningGetAlert(owner, repo, alertNumber, callback)</td>
    <td style="padding:15px">secret-scanning/get-alert</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/secret-scanning/alerts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretScanningUpdateAlert(owner, repo, alertNumber, body, callback)</td>
    <td style="padding:15px">secret-scanning/update-alert</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/secret-scanning/alerts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretScanningListLocationsForAlert(owner, repo, alertNumber, page, perPage, callback)</td>
    <td style="padding:15px">secret-scanning/list-locations-for-alert</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/secret-scanning/alerts/{pathv3}/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretScanningCreatePushProtectionBypass(owner, repo, body, callback)</td>
    <td style="padding:15px">secret-scanning/create-push-protection-bypass</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/secret-scanning/push-protection-bypasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsGetByName(org, teamSlug, callback)</td>
    <td style="padding:15px">teams/get-by-name</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsUpdateInOrg(org, teamSlug, body, callback)</td>
    <td style="padding:15px">teams/update-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsDeleteInOrg(org, teamSlug, callback)</td>
    <td style="padding:15px">teams/delete-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListDiscussionsInOrg(org, teamSlug, direction, perPage, page, pinned, callback)</td>
    <td style="padding:15px">teams/list-discussions-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsCreateDiscussionInOrg(org, teamSlug, body, callback)</td>
    <td style="padding:15px">teams/create-discussion-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsGetDiscussionInOrg(org, teamSlug, discussionNumber, callback)</td>
    <td style="padding:15px">teams/get-discussion-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsUpdateDiscussionInOrg(org, teamSlug, discussionNumber, body, callback)</td>
    <td style="padding:15px">teams/update-discussion-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsDeleteDiscussionInOrg(org, teamSlug, discussionNumber, callback)</td>
    <td style="padding:15px">teams/delete-discussion-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListDiscussionCommentsInOrg(org, teamSlug, discussionNumber, direction, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-discussion-comments-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsCreateDiscussionCommentInOrg(org, teamSlug, discussionNumber, body, callback)</td>
    <td style="padding:15px">teams/create-discussion-comment-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsGetDiscussionCommentInOrg(org, teamSlug, discussionNumber, commentNumber, callback)</td>
    <td style="padding:15px">teams/get-discussion-comment-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsUpdateDiscussionCommentInOrg(org, teamSlug, discussionNumber, commentNumber, body, callback)</td>
    <td style="padding:15px">teams/update-discussion-comment-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsDeleteDiscussionCommentInOrg(org, teamSlug, discussionNumber, commentNumber, callback)</td>
    <td style="padding:15px">teams/delete-discussion-comment-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/discussions/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListPendingInvitationsInOrg(org, teamSlug, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-pending-invitations-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/invitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListMembersInOrg(org, teamSlug, role, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-members-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsGetMembershipForUserInOrg(org, teamSlug, username, callback)</td>
    <td style="padding:15px">teams/get-membership-for-user-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/memberships/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsAddOrUpdateMembershipForUserInOrg(org, teamSlug, username, body, callback)</td>
    <td style="padding:15px">teams/add-or-update-membership-for-user-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/memberships/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsRemoveMembershipForUserInOrg(org, teamSlug, username, callback)</td>
    <td style="padding:15px">teams/remove-membership-for-user-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/memberships/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListProjectsInOrg(org, teamSlug, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-projects-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsCheckPermissionsForProjectInOrg(org, teamSlug, projectId, callback)</td>
    <td style="padding:15px">teams/check-permissions-for-project-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/projects/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsAddOrUpdateProjectPermissionsInOrg(org, teamSlug, projectId, body, callback)</td>
    <td style="padding:15px">teams/add-or-update-project-permissions-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/projects/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsRemoveProjectInOrg(org, teamSlug, projectId, callback)</td>
    <td style="padding:15px">teams/remove-project-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/projects/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListReposInOrg(org, teamSlug, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-repos-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/repos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsCheckPermissionsForRepoInOrg(org, teamSlug, owner, repo, callback)</td>
    <td style="padding:15px">teams/check-permissions-for-repo-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/repos/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsAddOrUpdateRepoPermissionsInOrg(org, teamSlug, owner, repo, body, callback)</td>
    <td style="padding:15px">teams/add-or-update-repo-permissions-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/repos/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsRemoveRepoInOrg(org, teamSlug, owner, repo, callback)</td>
    <td style="padding:15px">teams/remove-repo-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/repos/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListChildInOrg(org, teamSlug, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-child-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/teams/{pathv2}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListDiscussionsLegacy(teamId, direction, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-discussions-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsCreateDiscussionLegacy(teamId, body, callback)</td>
    <td style="padding:15px">teams/create-discussion-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsGetDiscussionLegacy(teamId, discussionNumber, callback)</td>
    <td style="padding:15px">teams/get-discussion-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsUpdateDiscussionLegacy(teamId, discussionNumber, body, callback)</td>
    <td style="padding:15px">teams/update-discussion-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsDeleteDiscussionLegacy(teamId, discussionNumber, callback)</td>
    <td style="padding:15px">teams/delete-discussion-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListDiscussionCommentsLegacy(teamId, discussionNumber, direction, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-discussion-comments-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsCreateDiscussionCommentLegacy(teamId, discussionNumber, body, callback)</td>
    <td style="padding:15px">teams/create-discussion-comment-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsGetDiscussionCommentLegacy(teamId, discussionNumber, commentNumber, callback)</td>
    <td style="padding:15px">teams/get-discussion-comment-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsUpdateDiscussionCommentLegacy(teamId, discussionNumber, commentNumber, body, callback)</td>
    <td style="padding:15px">teams/update-discussion-comment-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsDeleteDiscussionCommentLegacy(teamId, discussionNumber, commentNumber, callback)</td>
    <td style="padding:15px">teams/delete-discussion-comment-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/discussions/{pathv2}/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListPendingInvitationsLegacy(teamId, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-pending-invitations-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/invitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsTeamIdMembersQuery(teamId, role, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-members-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListProjectsLegacy(teamId, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-projects-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsCheckPermissionsForProjectLegacy(teamId, projectId, callback)</td>
    <td style="padding:15px">teams/check-permissions-for-project-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/projects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsAddOrUpdateProjectPermissionsLegacy(teamId, projectId, body, callback)</td>
    <td style="padding:15px">teams/add-or-update-project-permissions-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/projects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsRemoveProjectLegacy(teamId, projectId, callback)</td>
    <td style="padding:15px">teams/remove-project-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/projects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsTeamIdReposQuery(teamId, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-repos-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/repos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsListChildLegacy(teamId, perPage, page, callback)</td>
    <td style="padding:15px">teams/list-child-legacy</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesListInOrganization(perPage, page, org, callback)</td>
    <td style="padding:15px">codespaces/list-in-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesSetCodespacesAccess(org, body, callback)</td>
    <td style="padding:15px">codespaces/set-codespaces-access</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesSetCodespacesAccessUsers(org, body, callback)</td>
    <td style="padding:15px">codespaces/set-codespaces-access-users</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/access/selected_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesDeleteCodespacesAccessUsers(org, callback)</td>
    <td style="padding:15px">codespaces/delete-codespaces-access-users</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/access/selected_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesListOrgSecrets(org, perPage, page, callback)</td>
    <td style="padding:15px">codespaces/list-org-secrets</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesGetOrgPublicKey(org, callback)</td>
    <td style="padding:15px">codespaces/get-org-public-key</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/secrets/public-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesGetOrgSecret(org, secretName, callback)</td>
    <td style="padding:15px">codespaces/get-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesCreateOrUpdateOrgSecret(org, secretName, body, callback)</td>
    <td style="padding:15px">codespaces/create-or-update-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesDeleteOrgSecret(org, secretName, callback)</td>
    <td style="padding:15px">codespaces/delete-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesListSelectedReposForOrgSecret(org, secretName, page, perPage, callback)</td>
    <td style="padding:15px">codespaces/list-selected-repos-for-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/secrets/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesSetSelectedReposForOrgSecret(org, secretName, body, callback)</td>
    <td style="padding:15px">codespaces/set-selected-repos-for-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/secrets/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesAddSelectedRepoToOrgSecret(org, secretName, repositoryId, callback)</td>
    <td style="padding:15px">codespaces/add-selected-repo-to-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/secrets/{pathv2}/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesRemoveSelectedRepoFromOrgSecret(org, secretName, repositoryId, callback)</td>
    <td style="padding:15px">codespaces/remove-selected-repo-from-org-secret</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/codespaces/secrets/{pathv2}/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesGetCodespacesForUserInOrg(perPage, page, org, username, callback)</td>
    <td style="padding:15px">codespaces/get-codespaces-for-user-in-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/members/{pathv2}/codespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesDeleteFromOrganization(org, username, codespaceName, callback)</td>
    <td style="padding:15px">codespaces/delete-from-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/members/{pathv2}/codespaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesStopInOrganization(org, username, codespaceName, callback)</td>
    <td style="padding:15px">codespaces/stop-in-organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/members/{pathv2}/codespaces/{pathv3}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesListInRepositoryForAuthenticatedUser(perPage, page, owner, repo, callback)</td>
    <td style="padding:15px">codespaces/list-in-repository-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesCreateWithRepoForAuthenticatedUser(owner, repo, body, callback)</td>
    <td style="padding:15px">codespaces/create-with-repo-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesListDevcontainersInRepositoryForAuthenticatedUser(perPage, page, owner, repo, callback)</td>
    <td style="padding:15px">codespaces/list-devcontainers-in-repository-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codespaces/devcontainers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesRepoMachinesForAuthenticatedUser(owner, repo, location, clientIp, ref, callback)</td>
    <td style="padding:15px">codespaces/repo-machines-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codespaces/machines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesPreFlightWithRepoForAuthenticatedUser(owner, repo, ref, clientIp, callback)</td>
    <td style="padding:15px">codespaces/pre-flight-with-repo-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codespaces/new?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesCheckPermissionsForDevcontainer(owner, repo, ref, devcontainerPath, callback)</td>
    <td style="padding:15px">codespaces/check-permissions-for-devcontainer</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codespaces/permissions_check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesListRepoSecrets(owner, repo, perPage, page, callback)</td>
    <td style="padding:15px">codespaces/list-repo-secrets</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codespaces/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesGetRepoPublicKey(owner, repo, callback)</td>
    <td style="padding:15px">codespaces/get-repo-public-key</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codespaces/secrets/public-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesGetRepoSecret(owner, repo, secretName, callback)</td>
    <td style="padding:15px">codespaces/get-repo-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codespaces/secrets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesCreateOrUpdateRepoSecret(owner, repo, secretName, body, callback)</td>
    <td style="padding:15px">codespaces/create-or-update-repo-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codespaces/secrets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesDeleteRepoSecret(owner, repo, secretName, callback)</td>
    <td style="padding:15px">codespaces/delete-repo-secret</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/codespaces/secrets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesCreateWithPrForAuthenticatedUser(owner, repo, pullNumber, body, callback)</td>
    <td style="padding:15px">codespaces/create-with-pr-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/pulls/{pathv3}/codespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesListForAuthenticatedUser(perPage, page, repositoryId, callback)</td>
    <td style="padding:15px">codespaces/list-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesCreateForAuthenticatedUser(body, callback)</td>
    <td style="padding:15px">codespaces/create-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesListSecretsForAuthenticatedUser(perPage, page, callback)</td>
    <td style="padding:15px">codespaces/list-secrets-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesGetPublicKeyForAuthenticatedUser(callback)</td>
    <td style="padding:15px">codespaces/get-public-key-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/secrets/public-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesGetSecretForAuthenticatedUser(secretName, callback)</td>
    <td style="padding:15px">codespaces/get-secret-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesCreateOrUpdateSecretForAuthenticatedUser(secretName, body, callback)</td>
    <td style="padding:15px">codespaces/create-or-update-secret-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesDeleteSecretForAuthenticatedUser(secretName, callback)</td>
    <td style="padding:15px">codespaces/delete-secret-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesListRepositoriesForSecretForAuthenticatedUser(secretName, callback)</td>
    <td style="padding:15px">codespaces/list-repositories-for-secret-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/secrets/{pathv1}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesSetRepositoriesForSecretForAuthenticatedUser(secretName, body, callback)</td>
    <td style="padding:15px">codespaces/set-repositories-for-secret-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/secrets/{pathv1}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesAddRepositoryForSecretForAuthenticatedUser(secretName, repositoryId, callback)</td>
    <td style="padding:15px">codespaces/add-repository-for-secret-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/secrets/{pathv1}/repositories/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesRemoveRepositoryForSecretForAuthenticatedUser(secretName, repositoryId, callback)</td>
    <td style="padding:15px">codespaces/remove-repository-for-secret-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/secrets/{pathv1}/repositories/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesGetForAuthenticatedUser(codespaceName, callback)</td>
    <td style="padding:15px">codespaces/get-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesUpdateForAuthenticatedUser(codespaceName, body, callback)</td>
    <td style="padding:15px">codespaces/update-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesDeleteForAuthenticatedUser(codespaceName, callback)</td>
    <td style="padding:15px">codespaces/delete-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesExportForAuthenticatedUser(codespaceName, callback)</td>
    <td style="padding:15px">codespaces/export-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/{pathv1}/exports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesGetExportDetailsForAuthenticatedUser(codespaceName, exportId, callback)</td>
    <td style="padding:15px">codespaces/get-export-details-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/{pathv1}/exports/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesCodespaceMachinesForAuthenticatedUser(codespaceName, callback)</td>
    <td style="padding:15px">codespaces/codespace-machines-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/{pathv1}/machines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesPublishForAuthenticatedUser(codespaceName, body, callback)</td>
    <td style="padding:15px">codespaces/publish-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/{pathv1}/publish?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesStartForAuthenticatedUser(codespaceName, callback)</td>
    <td style="padding:15px">codespaces/start-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/{pathv1}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codespacesStopForAuthenticatedUser(codespaceName, callback)</td>
    <td style="padding:15px">codespaces/stop-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/codespaces/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotListCopilotSeatsForEnterprise(enterprise, page, perPage, callback)</td>
    <td style="padding:15px">copilot/list-copilot-seats-for-enterprise</td>
    <td style="padding:15px">{base_path}/{version}/enterprises/{pathv1}/copilot/billing/seats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotUsageMetricsForEnterprise(enterprise, since, until, page, perPage, callback)</td>
    <td style="padding:15px">copilot/usage-metrics-for-enterprise</td>
    <td style="padding:15px">{base_path}/{version}/enterprises/{pathv1}/copilot/usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotUsageMetricsForEnterpriseTeam(enterprise, teamSlug, since, until, page, perPage, callback)</td>
    <td style="padding:15px">copilot/usage-metrics-for-enterprise-team</td>
    <td style="padding:15px">{base_path}/{version}/enterprises/{pathv1}/team/{pathv2}/copilot/usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotGetCopilotOrganizationDetails(org, callback)</td>
    <td style="padding:15px">copilot/get-copilot-organization-details</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/copilot/billing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotListCopilotSeats(org, page, perPage, callback)</td>
    <td style="padding:15px">copilot/list-copilot-seats</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/copilot/billing/seats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotAddCopilotSeatsForTeams(org, body, callback)</td>
    <td style="padding:15px">copilot/add-copilot-seats-for-teams</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/copilot/billing/selected_teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotCancelCopilotSeatAssignmentForTeams(org, callback)</td>
    <td style="padding:15px">copilot/cancel-copilot-seat-assignment-for-teams</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/copilot/billing/selected_teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotAddCopilotSeatsForUsers(org, body, callback)</td>
    <td style="padding:15px">copilot/add-copilot-seats-for-users</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/copilot/billing/selected_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotCancelCopilotSeatAssignmentForUsers(org, callback)</td>
    <td style="padding:15px">copilot/cancel-copilot-seat-assignment-for-users</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/copilot/billing/selected_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotUsageMetricsForOrg(org, since, until, page, perPage, callback)</td>
    <td style="padding:15px">copilot/usage-metrics-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/copilot/usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotGetCopilotSeatDetailsForUser(org, username, callback)</td>
    <td style="padding:15px">copilot/get-copilot-seat-details-for-user</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/members/{pathv2}/copilot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copilotUsageMetricsForTeam(org, teamSlug, since, until, page, perPage, callback)</td>
    <td style="padding:15px">copilot/usage-metrics-for-team</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/team/{pathv2}/copilot/usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityAdvisoriesListGlobalAdvisories(ghsaId, type, cveId, ecosystem, severity, cwes, isWithdrawn, affects, published, updated, modified, epssPercentage, epssPercentile, before, after, direction, perPage, sort, callback)</td>
    <td style="padding:15px">security-advisories/list-global-advisories</td>
    <td style="padding:15px">{base_path}/{version}/advisories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityAdvisoriesGetGlobalAdvisory(ghsaId, callback)</td>
    <td style="padding:15px">security-advisories/get-global-advisory</td>
    <td style="padding:15px">{base_path}/{version}/advisories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityAdvisoriesListOrgRepositoryAdvisories(org, direction, sort, before, after, perPage, state, callback)</td>
    <td style="padding:15px">security-advisories/list-org-repository-advisories</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/security-advisories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityAdvisoriesListRepositoryAdvisories(owner, repo, direction, sort, before, after, perPage, state, callback)</td>
    <td style="padding:15px">security-advisories/list-repository-advisories</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/security-advisories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityAdvisoriesCreateRepositoryAdvisory(owner, repo, body, callback)</td>
    <td style="padding:15px">security-advisories/create-repository-advisory</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/security-advisories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityAdvisoriesCreatePrivateVulnerabilityReport(owner, repo, body, callback)</td>
    <td style="padding:15px">security-advisories/create-private-vulnerability-report</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/security-advisories/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityAdvisoriesGetRepositoryAdvisory(owner, repo, ghsaId, callback)</td>
    <td style="padding:15px">security-advisories/get-repository-advisory</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/security-advisories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityAdvisoriesUpdateRepositoryAdvisory(owner, repo, ghsaId, body, callback)</td>
    <td style="padding:15px">security-advisories/update-repository-advisory</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/security-advisories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityAdvisoriesCreateRepositoryAdvisoryCveRequest(owner, repo, ghsaId, callback)</td>
    <td style="padding:15px">security-advisories/create-repository-advisory-cve-request</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/security-advisories/{pathv3}/cve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityAdvisoriesCreateFork(owner, repo, ghsaId, callback)</td>
    <td style="padding:15px">security-advisories/create-fork</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/security-advisories/{pathv3}/forks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interactionsGetRestrictionsForOrg(org, callback)</td>
    <td style="padding:15px">interactions/get-restrictions-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/interaction-limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interactionsSetRestrictionsForOrg(org, body, callback)</td>
    <td style="padding:15px">interactions/set-restrictions-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/interaction-limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interactionsRemoveRestrictionsForOrg(org, callback)</td>
    <td style="padding:15px">interactions/remove-restrictions-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/interaction-limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interactionsGetRestrictionsForRepo(owner, repo, callback)</td>
    <td style="padding:15px">interactions/get-restrictions-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/interaction-limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interactionsSetRestrictionsForRepo(owner, repo, body, callback)</td>
    <td style="padding:15px">interactions/set-restrictions-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/interaction-limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interactionsRemoveRestrictionsForRepo(owner, repo, callback)</td>
    <td style="padding:15px">interactions/remove-restrictions-for-repo</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/interaction-limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interactionsGetRestrictionsForAuthenticatedUser(callback)</td>
    <td style="padding:15px">interactions/get-restrictions-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/interaction-limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interactionsSetRestrictionsForAuthenticatedUser(body, callback)</td>
    <td style="padding:15px">interactions/set-restrictions-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/interaction-limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interactionsRemoveRestrictionsForAuthenticatedUser(callback)</td>
    <td style="padding:15px">interactions/remove-restrictions-for-authenticated-user</td>
    <td style="padding:15px">{base_path}/{version}/user/interaction-limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">classroomGetAnAssignment(assignmentId, callback)</td>
    <td style="padding:15px">classroom/get-an-assignment</td>
    <td style="padding:15px">{base_path}/{version}/assignments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">classroomListAcceptedAssigmentsForAnAssignment(assignmentId, page, perPage, callback)</td>
    <td style="padding:15px">classroom/list-accepted-assigments-for-an-assignment</td>
    <td style="padding:15px">{base_path}/{version}/assignments/{pathv1}/accepted_assignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">classroomGetAssignmentGrades(assignmentId, callback)</td>
    <td style="padding:15px">classroom/get-assignment-grades</td>
    <td style="padding:15px">{base_path}/{version}/assignments/{pathv1}/grades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">classroomListClassrooms(page, perPage, callback)</td>
    <td style="padding:15px">classroom/list-classrooms</td>
    <td style="padding:15px">{base_path}/{version}/classrooms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">classroomGetAClassroom(classroomId, callback)</td>
    <td style="padding:15px">classroom/get-a-classroom</td>
    <td style="padding:15px">{base_path}/{version}/classrooms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">classroomListAssignmentsForAClassroom(classroomId, page, perPage, callback)</td>
    <td style="padding:15px">classroom/list-assignments-for-a-classroom</td>
    <td style="padding:15px">{base_path}/{version}/classrooms/{pathv1}/assignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeSecurityGetConfigurationsForOrg(org, targetType, perPage, before, after, callback)</td>
    <td style="padding:15px">code-security/get-configurations-for-org</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/code-security/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeSecurityCreateConfiguration(org, body, callback)</td>
    <td style="padding:15px">code-security/create-configuration</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/code-security/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeSecurityGetDefaultConfigurations(org, callback)</td>
    <td style="padding:15px">code-security/get-default-configurations</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/code-security/configurations/defaults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeSecurityDetachConfiguration(org, callback)</td>
    <td style="padding:15px">code-security/detach-configuration</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/code-security/configurations/detach?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeSecurityGetConfiguration(org, configurationId, callback)</td>
    <td style="padding:15px">code-security/get-configuration</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/code-security/configurations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeSecurityUpdateConfiguration(org, configurationId, body, callback)</td>
    <td style="padding:15px">code-security/update-configuration</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/code-security/configurations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeSecurityDeleteConfiguration(org, configurationId, callback)</td>
    <td style="padding:15px">code-security/delete-configuration</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/code-security/configurations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeSecurityAttachConfiguration(org, configurationId, body, callback)</td>
    <td style="padding:15px">code-security/attach-configuration</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/code-security/configurations/{pathv2}/attach?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeSecuritySetConfigurationAsDefault(org, configurationId, body, callback)</td>
    <td style="padding:15px">code-security/set-configuration-as-default</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/code-security/configurations/{pathv2}/defaults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeSecurityGetRepositoriesForConfiguration(org, configurationId, perPage, before, after, status, callback)</td>
    <td style="padding:15px">code-security/get-repositories-for-configuration</td>
    <td style="padding:15px">{base_path}/{version}/orgs/{pathv1}/code-security/configurations/{pathv2}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">codeSecurityGetConfigurationForRepository(owner, repo, callback)</td>
    <td style="padding:15px">code-security/get-configuration-for-repository</td>
    <td style="padding:15px">{base_path}/{version}/repos/{pathv1}/{pathv2}/code-security-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
