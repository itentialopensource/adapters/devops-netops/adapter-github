
## 0.7.2 [11-29-2024]

* Add call to push multiple files

See merge request itentialopensource/adapters/adapter-github!30

---

## 0.7.1 [10-15-2024]

* Changes made at 2024.10.14_21:18PM

See merge request itentialopensource/adapters/adapter-github!29

---

## 0.7.0 [09-27-2024]

* Minor/adapt 3703

See merge request itentialopensource/adapters/adapter-github!27

---

## 0.6.8 [08-26-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-github!25

---

## 0.6.7 [08-14-2024]

* Changes made at 2024.08.14_19:34PM

See merge request itentialopensource/adapters/adapter-github!24

---

## 0.6.6 [08-07-2024]

* Changes made at 2024.08.06_21:37PM

See merge request itentialopensource/adapters/adapter-github!23

---

## 0.6.5 [03-26-2024]

* Changes made at 2024.03.26_14:26PM

See merge request itentialopensource/adapters/devops-netops/adapter-github!21

---

## 0.6.4 [03-21-2024]

* Changes made at 2024.03.21_14:31PM

See merge request itentialopensource/adapters/devops-netops/adapter-github!20

---

## 0.6.3 [03-11-2024]

* Changes made at 2024.03.11_13:23PM

See merge request itentialopensource/adapters/devops-netops/adapter-github!19

---

## 0.6.2 [02-26-2024]

* Changes made at 2024.02.26_13:49PM

See merge request itentialopensource/adapters/devops-netops/adapter-github!18

---

## 0.6.1 [12-24-2023]

* update axios and metadata

See merge request itentialopensource/adapters/devops-netops/adapter-github!17

---

## 0.6.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/devops-netops/adapter-github!16

---

## 0.5.0 [11-07-2023]

* More migration changes

See merge request itentialopensource/adapters/devops-netops/adapter-github!16

---

## 0.4.4 [10-19-2023]

* fix meta links

See merge request itentialopensource/adapters/devops-netops/adapter-github!15

---

## 0.4.3 [09-27-2023]

* fix meta links

See merge request itentialopensource/adapters/devops-netops/adapter-github!15

---

## 0.4.2 [09-27-2023]

* fix meta links

See merge request itentialopensource/adapters/devops-netops/adapter-github!15

---

## 0.4.1 [09-27-2023]

* more meta changes

See merge request itentialopensource/adapters/devops-netops/adapter-github!14

---

## 0.4.0 [09-26-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-github!13

---

## 0.3.3 [06-07-2023]

* Patch/adapt 2390

See merge request itentialopensource/adapters/devops-netops/adapter-github!10

---

## 0.3.2 [04-06-2023]

* updated adapter utils ver

See merge request itentialopensource/adapters/devops-netops/adapter-github!11

---

## 0.3.1 [03-24-2023]

* updated adapter utils ver

See merge request itentialopensource/adapters/devops-netops/adapter-github!11

---

## 0.3.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-github!9

---

## 0.2.6 [08-02-2021]

- Change the healthcheck to something returning less data

See merge request itentialopensource/adapters/devops-netops/adapter-github!8

---

## 0.2.5 [03-22-2021]

- Add new call to adapter

See merge request itentialopensource/adapters/devops-netops/adapter-github!6

---

## 0.2.4 [03-17-2021]

- Add new required header to sampleProperties and need to encode the content for putReposOwnerRepoContentsPath

See merge request itentialopensource/adapters/devops-netops/adapter-github!5

---

## 0.2.3 [03-07-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/devops-netops/adapter-github!4

---

## 0.2.2 [07-08-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/devops-netops/adapter-github!3

---

## 0.2.1 [01-09-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/devops-netops/adapter-github!2

---

## 0.2.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/devops-netops/adapter-github!1

---

## 0.1.1 [09-20-2019]

- Initial Commit

See commit cb9b579

---
